//
//  AppConstants.swift
//  Mindwave
//
//  Created by Arijit Sarkar on 22/11/19.
//  Copyright © 2019 Avantari. All rights reserved.
//

import UIKit

struct AppConstants {

//    static let goodSessionResultText = "Great Job!\nYou have achieved calmness."
//    static let averageSessionResultText = "Almost there!\nWant to achieve more calmness?"
//    static let belowAverageSessionResultText = "Almost there!\n Good time to reduce your stress levels?"
//    static let notAGreatSessionResultText = "You have too much on your mind/ Want to achieve clamness?."
    static let contactInstructionText = "Talk to one of our Dhyanees to know more."
    static let interruptesSessionText = "Session was interrupted.\nPlease try again..."
    static let calmWaveCalmMindText = "Calm The Wave, Calm Your Mind."
    static let placeYourHandToStartText = "Place your hand on the sensor to start."
    static let discoveringHeartRateCharacteristicsText = "Discovering Heart Rate Characteristics..."
    static let placeYourFingerBack = "Please place your finger back on the sensor."
    static let calmYourMind = "Try to calm your mind."

}

struct AppColor {
    static let inactiveColor = UIColor.lightGray
    static let activeColor = UIColor.dhyanaBlue
}
