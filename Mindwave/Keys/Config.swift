//
//  Config.swift
//  Mindwave
//
//  Created by Arijit Sarkar on 15/11/19.
//  Copyright © 2019 Avantari. All rights reserved.
//

import Foundation

struct Config {

    static var termsandconditionsurl = "http://smartdhyana.com/termsofuse.html"
    static var privacypolicyurl = "http://smartdhyana.com/privacypolicy.html"

    static var emotion_data = "EMOTION_DATA"
    static var bearer_token = "BEARER_TOKEN"
    static var downloaded_files = "DOWNLOADED_FILES"
    static var user_logged_in = "USER_LOGGED_IN"
    static var device_connected = "DEVICE_CONNECTED"
    static var connected_device_name = "CONNECTED_DEVICE_NAME"
    static var current_device_id = "CURRENT_DEVICE_ID"
    static var unlock_free_emotions = "UNLOCK_FREE_EMOTIONS"
    static var single_emotion_data = "SINGLE_EMOTION_DATA"
    static var userLoggedInEmail = "USER_LOGGEDIN_EMAIL"
    static var firmwareZipUrl = "FIRMWARE_ZIP_URL"
    static var currencyCode = "CURRENCY_CODE"
    static var priceArray = "PRICE_ARRAY"

    static var DontShowDontDoInformation = "Dont Show Dont Do Information"
    static var DontShowDhyanaMeditationInformation = "DontShowDhyanaMeditationInformation"

    static var failureMessage: [String: Any] = ["message":"Server Error. Check your internet connection and try again."]

    static var batterPercentageKey = "BatteryPercentageKey"

    static var EmotionTimes = "EmotionTimes"

    static var GroupName = "group.avantari.dhyanagenric"

    static var SelectedMeditationTypeKey = "SelectedMeditationTypeKey"

    static var RING_Prefix = "RING_"

    static let sessionState = "sessionState"
    static let heartRateArray = "heartRateArray"
    static var mindfull_report_datapoints = "MINDFULL_REPORT_DATAPOINTS"

    static let isTesting = false
    static let noNeedRing = false

    static let SharingEnabled = true
    static let UserSharing = true

    static let viewTransitionDuration = 0.5
    static let viewAlphaDuration = 0.5

    static let appGroupName = "group.avantari.dhyana"
    
    static var screenRefreshRate = 60
    static var timeWeightageInResult = 0.8
}
