//
//  BackgroundLeafyView.swift
//  Mindwave
//
//  Created by Arijit Sarkar on 18/11/19.
//  Copyright © 2019 Avantari. All rights reserved.
//

import UIKit

class LeafyBackgroundView: UIView {

    //MARK: IBOutlets
    @IBOutlet var leafImageViews: [UIImageView]!

    // MARK: Properties
    private var contentView: UIView!

    // MARK: LifeCycle Methods
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }

    // MARK: Helper Methods
    private func setupView() {
        if contentView == nil {
            guard let reqView = loadViewFromXIB() else { return }
            contentView = reqView
            contentView.backgroundColor = .clear
            self.addSubview(contentView)
            contentView.frame = self.bounds
            contentView.fillSuperview()
            backgroundColor = .clear
        }
    }

    private func loadViewFromXIB() -> UIView? {
        return Bundle.main.loadNibNamed("BackgroundLeafyView", owner: self, options: nil)?.first as? UIView
    }

    func beginAnimations() {
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`BEGIN LEAF ANIMATION")
        for index in 0..<leafImageViews.count {
            let view = leafImageViews[index]
            if index < 3 {

                UIView.animate(withDuration: 0.2 * Double(index + 2) + 3.0, delay: 0, options: [.repeat, .autoreverse], animations: {
                    view.transform = view.transform.rotated(by: -.pi/20)
                }, completion: { _ in
                    UIView.animate(withDuration: 0.2 * Double(index + 2) + 3.0, animations: {
                        view.transform = .identity
                    }, completion: nil)
                })
            } else {
                UIView.animate(withDuration: 0.2 * Double(index + 2) + 3.0, delay: 0, options: [.repeat, .autoreverse], animations: {
                    view.transform = view.transform.rotated(by: .pi / 12)
                }, completion: { _ in
                    UIView.animate(withDuration: 0.2 * Double(index + 2) + 3.0, animations: {
                        view.transform = .identity
                    }, completion: nil)
                })
            }
        }
    }

    func endAnimations() {
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`END LEAF ANIMATION")
        for index in 0..<leafImageViews.count {
            let view = leafImageViews[index]
            view.transform = .identity
            view.layer.removeAllAnimations()
        }
    }

}
