//
//  InOutCircle.swift
//  InOutCircle
//
//  Created by Hari Keerthipati on 26/06/18.
//  Copyright © 2018 Avantari Technologies. All rights reserved.
//

import UIKit

class InOutLine: UIView {
    
    private var leftCurve = CAShapeLayer()
    private var rightCurve = CAShapeLayer()
    var trackLayer = CAShapeLayer()
    var label: UILabel!
    
    private var circularPath: UIBezierPath?
    private var secondCircularPath: UIBezierPath?
    private let fromValue = 0.0
    private let toValue = 1.0
    
    open var layerColor: UIColor = .dhyanaBlue
        {
        didSet
        {
            leftCurve.strokeColor = layerColor.cgColor
            rightCurve.strokeColor = layerColor.cgColor
        }
    }
    open var trackColor: UIColor = .white
        {
        didSet
        {
            trackLayer.strokeColor = trackColor.cgColor
        }
    }
    
    open var lineWidth:CGFloat = 14.0
    
    //MARK: FUNCTIONS
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupPaths() {
        
        self.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
        
        let trackPath = UIBezierPath(roundedRect: CGRect(x: 0, y: self.bounds.height - (3/2) * lineWidth, width: self.bounds.width, height: lineWidth), cornerRadius: lineWidth)
        trackLayer = CAShapeLayer()
        trackLayer.path = trackPath.cgPath
        
        trackLayer.strokeColor = UIColor.dhyanaBlue.cgColor
        trackLayer.lineWidth = 1.0
        trackLayer.fillColor = UIColor.clear.cgColor
        trackLayer.lineCap = .round
        trackLayer.lineJoin = .round
        self.layer.addSublayer(trackLayer)
        
        circularPath = UIBezierPath()//arcCenter: .zero, radius: self.bounds.height / 2, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
        circularPath?.move(to: CGPoint(x: self.bounds.width / 2, y: self.bounds.height - lineWidth))
        circularPath?.addLine(to: CGPoint(x: 7.0, y: self.bounds.height - lineWidth))
        leftCurve = CAShapeLayer()
        leftCurve.path = circularPath?.cgPath
        leftCurve.strokeColor = layerColor.cgColor
        leftCurve.lineWidth = lineWidth
        leftCurve.fillColor = layerColor.cgColor
        leftCurve.lineCap = CAShapeLayerLineCap.round
        leftCurve.strokeEnd = 0
        self.layer.addSublayer(leftCurve)
        
        secondCircularPath = UIBezierPath()//arcCenter: .zero, radius: self.bounds.height / 2, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: false)
        secondCircularPath?.move(to: CGPoint(x: self.bounds.width / 2, y: self.bounds.height - lineWidth))
        secondCircularPath?.addLine(to: CGPoint(x: self.bounds.width - 7.0, y: self.bounds.height - lineWidth))
        rightCurve = CAShapeLayer()
        rightCurve.path = secondCircularPath?.cgPath
        rightCurve.strokeColor = layerColor.cgColor
        rightCurve.lineWidth = lineWidth
        rightCurve.fillColor = layerColor.cgColor
        rightCurve.lineCap = CAShapeLayerLineCap.round
        rightCurve.strokeEnd = 0
        self.layer.addSublayer(rightCurve)

        label = UILabel(frame: CGRect(x: 0, y: 0, width: bounds.width, height: 30.0))
        label.textAlignment = .center
        label.textColor = .dhyanaBlue
        label.font = UIFont.systemFont(ofSize: 30.0)
        addSubview(label)
    }
    
    func fillCirlce(duration: Double, completion: @escaping () -> Void){
        
        print("===inhaling===")
        setText(to: "Inhale")
        startAnimation(from: fromValue, to: toValue, duration: duration) {
            completion()
        }
    }
    
    func unfillCirlce(duration: Double, completion: @escaping () -> Void){
        
        print("===exhaling===")
        setText(to: "Exhale")
        startAnimation(from: toValue, to: fromValue, duration: duration) {
            completion()
        }
    }
    
    func reset(completion: @escaping () -> Void)
    {
        let currentScale = leftCurve.presentation()?.value(forKeyPath: "strokeEnd") ?? 0.0
        print("current value==\(currentScale)")
        startAnimation(from: currentScale as! Double, to: fromValue, duration: 0) {
            print("finished resetting")
        }
    }
    
    private func startAnimation(from: Double, to: Double, duration: Double, completion: @escaping () -> Void)
    {
        leftCurve.beginTime = 0
        rightCurve.beginTime = 0
        CATransaction.begin()
        CATransaction.setCompletionBlock({
            completion()
        })
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.fromValue = from
        animation.toValue = to
        animation.duration = duration
        animation.fillMode = CAMediaTimingFillMode.both
        
        animation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        animation.isRemovedOnCompletion = false
        self.leftCurve.add(animation, forKey: "strokeEnd")
        self.rightCurve.add(animation, forKey: "strokeEnd")
        CATransaction.commit()
    }
    
    func pauseAnimations()
    {
        leftCurve.pauseAnimation()
        rightCurve.pauseAnimation()
        label.textColor = .lightGray
        changeStrokeColor(forLayer: trackLayer, to: UIColor.lightGray.cgColor)
        changeStrokeColor(forLayer: leftCurve, to: UIColor.lightGray.cgColor)
        changeStrokeColor(forLayer: rightCurve, to: UIColor.lightGray.cgColor)
    }
    
    func resumeAnimations()
    {
        leftCurve.resumeAnimation()
        rightCurve.resumeAnimation()
        label.textColor = .dhyanaBlue
        changeStrokeColor(forLayer: trackLayer, to: UIColor.dhyanaBlue.cgColor)
        changeStrokeColor(forLayer: leftCurve, to: UIColor.dhyanaBlue.cgColor)
        changeStrokeColor(forLayer: rightCurve, to: UIColor.dhyanaBlue.cgColor)
    }
    
    func removeAllAnimations()
    {
        leftCurve.removeAllAnimations()
        rightCurve.removeAllAnimations()
    }

    func setText(to: String) {
        label.text = to
    }

    func changeStrokeColor(forLayer layer: CAShapeLayer, to: CGColor) {
        layer.strokeColor = to
        let animcolor = CABasicAnimation(keyPath: "strokeColor")
        animcolor.fromValue = layer.strokeColor ?? UIColor.dhyanaBlue.cgColor
        animcolor.toValue = to
        animcolor.duration = 0.2;
        animcolor.repeatCount = 0;
        animcolor.autoreverses = false
        layer.add(animcolor, forKey: "strokeColor")
    }

}

extension CAShapeLayer {

    func pauseAnimation(){
        let pausedTime = self.convertTime(CACurrentMediaTime(), from: nil)
        self.speed = 0.0
        self.timeOffset = pausedTime
    }
    
    func resumeAnimation(){
        let pausedTime = self.timeOffset
        self.speed = 1.0
        self.timeOffset = 0.0
        self.beginTime = 0.0
        let timeSincePause = self.convertTime(CACurrentMediaTime(), from: nil) - pausedTime
        self.beginTime = timeSincePause
    }

}
