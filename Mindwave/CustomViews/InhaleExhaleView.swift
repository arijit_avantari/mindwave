//
//  InhaleExhaleProgressView.swift
//  Mindwave
//
//  Created by Arijit Sarkar on 15/11/19.
//  Copyright © 2019 Avantari. All rights reserved.
//

import UIKit

class InhaleExhaleView: UIView {

    private var stackView: UIStackView!
    private var breathingStateLabel: UILabel!
    private var progressHolderView: UIView!
    private var progressIndicatorViews = [UIView]()
    private var countOfProgressIndicatorViews: Int = 3
    private var currentBreathingState: BreathingState = .inhale
    private var timer: SessionTimer?
    var heightofProgressView = CGFloat(14.0)
    private var progressIndicatorView: UIView!
    private var progressContainerView: UIView!
    private var elongateShrinkPropertyAnimator: UIViewPropertyAnimator?
    var initialProgressWidth = CGFloat(14)
    var elongateAnimationDuration: TimeInterval = 3
    var shrinkAnimationDuration: TimeInterval = 3
    var numberOfIterations = 2
    var currentIterationNumber = 0
    private var isRunningElongateAnimation = true
    private var animationStarted = false

    enum BreathingState: String {
        case inhale = "Inhale", exhale = "Exhale"
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
    }

    func setupUI() {

        subviews.forEach { $0.removeFromSuperview() }

        breathingStateLabel = UILabel(frame: CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height - (30 + heightofProgressView)))
        breathingStateLabel.text = currentBreathingState.rawValue
        breathingStateLabel.font = UIFont.systemFont(ofSize: 30.0)
        breathingStateLabel.textColor = UIColor.dhyanaBlue
        breathingStateLabel.textAlignment = .center

        progressHolderView = getProgressView()

        let view = UIView(frame: bounds)
        view.backgroundColor = .clear
        view.addSubview(breathingStateLabel)
        view.addSubview(progressHolderView)

        addSubview(view)
    }

    func getProgressView() -> UIView {

        progressContainerView = UIView(frame: CGRect(x: 0, y: bounds.height - heightofProgressView, width: bounds.width, height: heightofProgressView))
        progressContainerView.layer.borderWidth = 1.0
        progressContainerView.layer.borderColor = UIColor.dhyanaBlue.cgColor
        progressContainerView.layer.cornerRadius = CGFloat(7.0)

        progressIndicatorView = UIView(frame: progressContainerView.bounds)
        progressIndicatorView.layer.cornerRadius = CGFloat(7.0)
        progressIndicatorView.backgroundColor = UIColor.dhyanaBlue
        progressContainerView.addSubview(progressIndicatorView)
        progressIndicatorView.frame.size.width = initialProgressWidth
        progressIndicatorView.center = CGPoint(x: progressContainerView.bounds.midX, y: progressContainerView.bounds.midY)
        setElongateShrinkAnimator()
        return progressContainerView
    }

    func setElongateShrinkAnimator() {
        elongateShrinkPropertyAnimator = nil
        elongateShrinkPropertyAnimator = UIViewPropertyAnimator(duration: isRunningElongateAnimation ? elongateAnimationDuration : shrinkAnimationDuration, curve: .linear, animations: {
            print("/////////////// Elongation Animation Started")
            if self.isRunningElongateAnimation {
                self.setBreathingState(to: .inhale)
                self.progressIndicatorView.frame.size.width = self.bounds.width
                self.progressIndicatorView.center = CGPoint(x: self.progressContainerView.bounds.midX, y: self.progressContainerView.bounds.midY)
            } else {
                print("/////////////// Shrink Animation finished")
                self.setBreathingState(to: .exhale)
                self.progressIndicatorView.frame.size.width = self.initialProgressWidth
                self.progressIndicatorView.center = CGPoint(x: self.progressContainerView.bounds.midX, y: self.progressContainerView.bounds.midY)
            }
        })
        elongateShrinkPropertyAnimator?.addCompletion { (position) in
            if position == .end {
                if self.isRunningElongateAnimation {
                    print("/////////////// Elongation Animation finished")
                } else {
                    print("/////////////// Shrink Animation finished")
                }
                self.isRunningElongateAnimation = !self.isRunningElongateAnimation
                DispatchQueue.main.async {
                    self.stop()
                    self.currentIterationNumber += 1
                    if self.currentIterationNumber < self.numberOfIterations {
                        self.setElongateShrinkAnimator()
                        self.elongateShrinkPropertyAnimator?.startAnimation()
                    } else {
                        self.resetProgress()
                    }
                }
            }
        }
    }

    func animate() {
        print("ANIMATE")
        animationStarted = true
        activateInactivateViewLook(shouldActivate: true)
        elongateShrinkPropertyAnimator?.startAnimation()
    }

    func resume() {
        if animationStarted {
            print("RESUME")
            activateInactivateViewLook(shouldActivate: true)
            elongateShrinkPropertyAnimator?.startAnimation()
        }
    }

    func pause() {
        print("PAUSE")
        activateInactivateViewLook(shouldActivate: false)
        elongateShrinkPropertyAnimator?.pauseAnimation()
    }

    func stop() {
        print("STOP")
        elongateShrinkPropertyAnimator?.stopAnimation(true)
//        elongateShrinkPropertyAnimator?.finishAnimation(at: .end)
    }

    func setBreathingState(to: BreathingState) {
        currentBreathingState = to
        breathingStateLabel.text = to.rawValue
    }

    private func activateInactivateViewLook(shouldActivate: Bool) {
        progressContainerView.layer.borderColor = shouldActivate ? AppColor.activeColor.cgColor : AppColor.inactiveColor.cgColor
        progressIndicatorView.backgroundColor = shouldActivate ? AppColor.activeColor : AppColor.inactiveColor
        breathingStateLabel.textColor = shouldActivate ? AppColor.activeColor : AppColor.inactiveColor
    }

    func resetProgress() {
        print("\n\n\n\n RESET CALLED \n\n\n\n")
        UIView.animate(withDuration: 0.3) { self.alpha = 0.0 }
        animationStarted = false
        isRunningElongateAnimation = true
        currentIterationNumber = 0
        elongateShrinkPropertyAnimator = nil
        progressIndicatorView.frame.size.width = initialProgressWidth
        progressIndicatorView.center = CGPoint(x: self.progressContainerView.bounds.midX, y: self.progressContainerView.bounds.midY)
        setElongateShrinkAnimator()
    }

}
