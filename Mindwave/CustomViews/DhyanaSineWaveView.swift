//
//  DhyanaSineWaveView.swift
//  Mindwave
//
//  Created by Arijit Sarkar on 15/11/19.
//  Copyright © 2019 Avantari. All rights reserved.
//

import UIKit

let kDefaultFrequency:CGFloat = 1.5
let kDefaultAmplitude:CGFloat = 1.0
let kDefaultIdleAmplitude:CGFloat = 0.01
let kDefaultNumberOfWaves:Int = 5

var kDefaultPhaseShift:CGFloat {
    return SessionConfiguration.shared.sineWavePhaseShiftValue.cgfloat
}
let kDefaultDensity:CGFloat = 5.0
let kDefaultPrimaryLineWidth:CGFloat = 3.0
let kDefaultSecondaryLineWidth:CGFloat = 1.0

class DhyanaSineWaveView: UIView {

    var numberOfWaves:Int = 5

    @IBInspectable var  waveColor = UIColor.dhyanaBlue
    @IBInspectable var  primaryWaveLineWidth = kDefaultPrimaryLineWidth
    @IBInspectable var secondaryWaveLineWidth = kDefaultSecondaryLineWidth
    @IBInspectable var idleAmplitude:CGFloat = 0.0
    @IBInspectable var frequency = kDefaultFrequency
    @IBInspectable var amplitude = kDefaultAmplitude
    private var amplitudeSmooth:CGFloat?
    @IBInspectable var density = kDefaultPhaseShift
    @IBInspectable var phaseShift = kDefaultPhaseShift
    var phase:CGFloat = 0
    var displayLink: CADisplayLink!

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }

    func setup() {
        self.waveColor = UIColor.dhyanaBlue
        self.frequency = kDefaultFrequency
        self.amplitude = 0.0
        self.amplitudeSmooth = 0.0
        self.numberOfWaves = kDefaultNumberOfWaves
        self.phaseShift = kDefaultPhaseShift
        self.density = kDefaultDensity
        self.primaryWaveLineWidth = kDefaultPrimaryLineWidth
        self.secondaryWaveLineWidth = kDefaultSecondaryLineWidth
        self.phase = 0
    }

    func updateWithLevel(level:CGFloat, baseLevel: CGFloat = 0.1) {

        self.phase += self.phaseShift
        self.amplitude = fmax(baseLevel, level)
        self.setNeedsDisplay()
    }

    @objc func updateSineWavesAmplitude() {
        self.updateWithLevel(level: CGFloat(self.amplitude))
    }

    func endAnimations()
    {
        self.phase += self.phaseShift
        self.amplitude = 0.0
        self.setNeedsDisplay()
    }

    func startAnimating() {
        stopAnimating()
        displayLink = CADisplayLink(target: self, selector: #selector(self.updateSineWavesAmplitude))
        displayLink.add(to: RunLoop.current, forMode: RunLoop.Mode.default)
    }

    func stopAnimating()
    {
        if displayLink != nil{
            displayLink.remove(from: RunLoop.current, forMode: RunLoop.Mode.default)
            displayLink.invalidate()
            displayLink = nil
        }
    }

    override func draw(_ rect: CGRect) {

        self.amplitudeSmooth = self.amplitudeSmooth! +  (self.amplitude - self.amplitudeSmooth!)/40
        let context:CGContext = UIGraphicsGetCurrentContext()!
        context.clear(self.bounds);
        UIColor.white.set()
        context.fill(rect)

        for i in 0..<self.numberOfWaves {

            context.setLineWidth((i == 0 ? self.primaryWaveLineWidth : self.secondaryWaveLineWidth))

            let halfHeight = self.bounds.height / 2.0
            let fullwidth = self.bounds.width
            let mid = fullwidth/2.0
            let maxAmplitude = halfHeight - 4.0;
            let progress = 1.0 - Float(i) / Float(self.numberOfWaves)
            let normedAmplitude = CGFloat(1.5 * progress - 0.5) * amplitudeSmooth!
            let multiplier = CGFloat(fminf(1.0, (progress / 3.0 * 2.0) + (1.0 / 3.0)));

            let alpha = multiplier * self.waveColor.cgColor.alpha
            self.waveColor.withAlphaComponent(alpha).set()
            var x:CGFloat = 0.0
            var y:CGFloat = 0.0
            while x < (fullwidth + self.density) {
                let scaling = -pow(1 / mid * (x - mid), 2) + 1
                var amplitude = scaling * maxAmplitude * normedAmplitude
                amplitude = fmin(amplitude, maxAmplitude)

                if (i % 2 == 0) {
                    y = amplitude * CGFloat( cosf(Float (2 * Double.pi) * Float (x / fullwidth) * Float( self.frequency ) + Float(self.phase)) ) + halfHeight
                } else {
                    y = amplitude * CGFloat( sinf(Float (2 * Double.pi) * Float (x / fullwidth) * Float( self.frequency ) + Float(self.phase)) ) + halfHeight
                }
                if (x == 0) {
                    context.move(to: CGPoint(x: x, y: y))
                } else {
                    context.addLine(to: CGPoint(x: x, y: y))
                }
                x += self.density
            }
            context.strokePath();
        }
    }
}
