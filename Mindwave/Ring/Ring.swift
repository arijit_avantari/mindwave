//
//  Ring.swift
//  DhyanaNew
//
//  Created by Hari Keerthipati on 22/01/19.
//  Copyright © 2019 Avantari Technologies. All rights reserved.
//

import UIKit

enum RingErrorState {
    case notConnected
    case noHeartRates
    case noSignal
    case fingerNotDetected
}

struct Signal {
    var signalStrength: Int
    var averageSignalPercentage: Int
    var range: Int
    var stability: Int
    var isCharging: Bool
    var fingerDetected: Bool
}

enum SessionState {
    case notStarted, initiated, running, pausedIntentionally, pausedTechinicalReasons
}

enum BatteryState {
    case charging
    case low
    case good
}

struct RingUpdate {
    var link: String
    var firmwareVersion: String
}

class Ring: NSObject {
    
    static var shared: Ring = Ring()
    var connected = false
    var signalStrength: Int = 0
    var averageSignalStrengthPercentage: Int = 0
    var signalRange:Int = 0
    
    var stability: Int = 0
    var battery: Int = 0
    var isCharging: Bool = false
    var fingerDetected: Bool = false
    var isUpdateAvailable = false
    var signalDelegate: RingSignalDelegate?
    var hrDelegate: RingHeartRateDelegate?
    
    var latestVersionNumber : String = ""
    var isFirmwareAvailable:Bool = false
    var last3Values = [Int]()
    var isSignalGood = false
    var lastTimeStamp: Int = 0
    var ringStatusInfo: ((String)->())?
    var sessionState: SessionState = .notStarted
    var hardwareVersion: String?
    var firmwareVersion: String?
    var advertisementData: [String : Any]?
    var fromDFUUpdate = false
    var ringUpdate: RingUpdate?
    
    override init() {
        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(blueToothState(central:)), name: .DidChangeBlueToothState, object: nil)
    }
    
    func ringStatusUpdate(completion:((String)->())?) {
        
        ringStatusInfo = completion
    }
    
    func isSignalStrengthGood() -> Bool
    {
        return (signalStrength > 3)
    }
    
    func newFirmwareUpdateAvailable(link: String, fwVersion: String)
    {
        self.ringUpdate = RingUpdate(link: link, firmwareVersion: fwVersion)
    }
    var firstTime = false

    static func isConnected() -> Bool
    {
        return Ring.shared.connected
    }
    
    static func isPaired() -> Bool
    {
        return (UserDefaults.standard.string(forKey: Config.connected_device_name) != nil)
    }
    
    func canStartSession() -> Bool
    {
        return (RingConnectionManager.sharedManager.bluetoothConnected && connected && fingerDetected)
    }
    
    func isFingerDetected() -> Bool
    {
        return fingerDetected
    }
    
    static func startedSession() {
        Ring.shared.isSignalGood = true
    }
    
    static func finishedSession()
    {

    }
    
    static func startGettingHeartRates()
    {
        print("=======startGettingHeartRates======")
        RingConnectionManager.sharedManager.startGettingHeartRates()
    }
    
    static func stopGettingHeartRates()
    {
        print("=======stopGettingHeartRates======")
        RingConnectionManager.sharedManager.stopGettingHeartRates()
    }
    
    static func startConnection()
    {
        RingConnectionManager.sharedManager.startBLEConnection()
    }
    
    static func isUpdateAvailable() -> Bool {
        return Ring.shared.isUpdateAvailable
    }
    
    static func unpairDevice(completion: ((Bool)->())? = nil) {
    
        RingConnectionManager.sharedManager.unpairDevice { (success) in
            if let completion = completion
            {
                completion(success)
            }
        }
    }
    
    func resetRingStatus()
    {
        connected = false
        signalStrength = 0
        stability = 0
        battery = 0
        isCharging = false
        fingerDetected = false
        isSignalGood = false
        RingConnectionManager.sharedManager.resetConnection()
        last3Values.removeAll()
    }

}
