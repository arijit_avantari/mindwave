//
//  Ring.swift
//  DhyanaNew
//
//  Created by Hari Keerthipati on 19/11/18.
//  Copyright © 2018 Avantari Technologies. All rights reserved.
//

import UIKit
import CoreBluetooth

protocol RingSignalDelegate {
    func bluetoothTurnedOff()
    func bluetoothTurnedOn()
    func ringDisconnected()
    func ringConnected()
    func fingerNotDetected()
    func fingerDetected()
    func startedGettingSignal()
    func stoppedGettingSignal()
}

protocol RingHeartRateDelegate {

    func stoppedGettingHeartRates()
    func heartRateRecieved(heartRate: Double, timeStamp: Double)
}

extension Ring {
    
    @objc func blueToothState(central: CBCentralManager) {
        
        if RingConnectionManager.sharedManager.bluetoothConnected == false
        {
    
            self.signalDelegate?.bluetoothTurnedOff()
        }
    }
    
    func fingerDetectionUpdate(fingerDetected: Bool)
    {
        if self.fingerDetected != fingerDetected
        {
            self.fingerDetected = fingerDetected
//            if intensionallyPaused == false
//            {
                if fingerDetected {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                        if self.fingerDetected
                        {
                            self.signalDelegate?.fingerDetected()
                        }
                    }
                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                        if self.fingerDetected == false{
                            self.signalDelegate?.fingerNotDetected()
                        }
                    }
                }
     //       }
        }
    }
    
    @objc func calculateAverageSignalStrength(strength: Int)
    {
        if last3Values.count >= 3
        {
            last3Values.remove(at: 0)
            last3Values.append(strength)
            let filteredValues = self.inRangeValues()
            if filteredValues.count == 0
            {
                if sessionState == .running
                {
                    self.signalDelegate?.stoppedGettingSignal()
                }
            }
            else if filteredValues.count == 3
            {
                if sessionState == .pausedTechinicalReasons
                {
                    self.signalDelegate?.startedGettingSignal()
                }
            }
        }
        else
        {
            last3Values.append(strength)
        }
        averageSignalStrengthPercentage = (last3Values.floatAverage * 10).rounded().int
        signalRange = averageSignalStrengthPercentage.signalRange
    }
    
    func updateSessionState(_ state: SessionState)
    {
        self.sessionState = state
    }
    
    func pausedSession()
    {
        Ring.stopGettingHeartRates()
    }
    
    func resumeSession()
    {
        Ring.startGettingHeartRates()
    }
    
    func inRangeValues() -> [Int]
    {
        var filteredValues = [Int]()
        for value in last3Values
        {
            if value > 3
            {
                filteredValues.append(value)
            }
        }
        return filteredValues
    }
    
    func recievedHeartRate(data: Data) {
        
        if let heartRate = data.heartRate()
        {
            self.hrDelegate?.heartRateRecieved(heartRate: heartRate.0, timeStamp: heartRate.1)
        }
    }
    
    func recievedBattery(data: Data) {
        
        Ring.shared.battery = data.battery()
        NotificationCenter.default.post(name: .DidUpdatedBatteryPercentage, object: Ring.shared.battery)
    }
    
    func recievedSignalStrength(data: Data) {
        
        let ringStatus = data.ringStatus()
        signalStrength = ringStatus.signalStrength
        stability = ringStatus.stability
        if isCharging != ringStatus.isCharging
        {
            isCharging = ringStatus.isCharging
            NotificationCenter.default.post(name: .DidUpdatedChargingState, object: nil)
        }
        fingerDetectionUpdate(fingerDetected: ringStatus.fingerDetection)
        calculateAverageSignalStrength(strength: signalStrength)
        if let ringStatusInfo = self.ringStatusInfo
        {
            let hexString = data.getHexString()
            ringStatusInfo(hexString)
        }
        let signal = Signal(signalStrength: signalStrength, averageSignalPercentage: averageSignalStrengthPercentage, range: signalRange, stability: stability, isCharging: isCharging, fingerDetected: fingerDetected)
        NotificationCenter.default.post(name: .DidUpdatedSignalStrength, object: signal)
    }
    
    func compareVersionStrings( currentVersionString : String, latesVersionString : String ) -> Int {
        
        if currentVersionString == "" || latesVersionString == "" || currentVersionString == latesVersionString {
            return 0
        }
        let currentString = currentVersionString.components(separatedBy: ".")
        let latestString = latesVersionString.components(separatedBy: ".")
        let minLength = min(currentString.count, latestString.count)
        
        for i in 0..<minLength{
            if (Int(currentString[i])! > Int(latestString[i])!) {
                return -1;
            } else if (Int(currentString[i])! < Int(latestString[i])!) {
                return 1;
            }
        }
        return 0
    }
    
    func sessionFinished()
    {
        RingConnectionManager.sharedManager.stopGettingHeartRates()
        self.hrDelegate = nil
        self.signalDelegate = nil
    }
}


private extension Int {
    var signalRange: Int {
        switch self {
        case 0..<10:
            return 0
        case 10..<30:
            return 1
        case 30..<50:
            return 2
        default:
            return 3
        }
    }
}
