 //
 //  RingConnectionManager.swift
 //  DhyanaGenric
 //
 //  Created by AVANTARI on 02/11/17.
 //  Copyright © 2017 AVANTARI. All rights reserved.
 //
 
 import Foundation
 import UIKit
 import CoreBluetooth
 
 class RingConnectionManager: NSObject, CBCentralManagerDelegate, CBPeripheralDelegate {
    
    let FIRMWARE_VERSION_UUID = CBUUID(string: "2A26")
    let HARDWARE_VERSION_UUID = CBUUID(string: "2A27")
    let BLOOD_PRESSURE_UUID = CBUUID(string: "1810")
    let HEART_RATE_WITH_TIMESTAMP = CBUUID(string: "6f6e6d6c-6b6a-6968-6766-656463626160")
    let FLIP_BATTERY = CBUUID(string: "00002a19-0000-1000-8000-00805f9b34fb")
    let FLIP_SYSYEM_CMD_UUID = CBUUID(string: "4f4e4d4c-4b4a-4948-4746-454443424140")
    let FLIP_SENSOR_DATA_SERVICE_UUID = CBUUID(string: "0f0e0d0c-0b0a-0908-0706-050403020100")
    let DEVICE_STATUS_INFORMATION = CBUUID(string: "7F7E7D7C-7B7A-7978-7776-757473727170")
    
    var allPairedIds = [NSUUID]()
    var logChar: CBCharacteristic!
    var notifyToControllerOnConnection = false
    var peripherals = [CBPeripheral]()
    
    static let sharedManager = RingConnectionManager()
      
    // For OTA
    var centralManager:CBCentralManager!
    var connectedPeripheral: CBPeripheral?
    var ringName: String?
    var last3Values = [Int]()
    
    private static var fileZipUrl: String?
    private var shouldStopReconnection = false
    
    // Callbacks
    var bluetoothStateCallBack: ((CBCentralManager)->())?
    var continuousDeviceConnectionState: ((Bool)->())?
    
    var bluetoothConnected = false
    var heartRateCharacteristic: CBCharacteristic?

    override init() {
        super.init()
    }
    
    func startBLEConnection() {
        centralManager = CBCentralManager(delegate: self, queue: nil, options: [CBCentralManagerOptionShowPowerAlertKey : [NSNumber(value: 1)]])
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        
        switch central.state {
        case .poweredOn:

            central.scanForPeripherals(withServices: nil, options: [CBCentralManagerScanOptionAllowDuplicatesKey:false])
            bluetoothConnected = true
            NotificationCenter.default.post(name: .DidChangeBlueToothState, object: central)

        default:

            bluetoothConnected = false
            Ring.shared.connected = false
            Ring.shared.fingerDetected = false
            NotificationCenter.default.post(name: .DidChangeBlueToothState, object: central)
            NotificationCenter.default.post(name: .DidChangeDeviceConnectionState, object: false)
            break
        }
    }

    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        print("peripheral.name===\(peripheral.name ?? "no name") ---- \(RSSI.intValue)")
        guard let peripheralName = peripheral.name,
            peripheralName.hasPrefix("RING") || peripheralName.hasPrefix("Dhyana") || peripheralName.hasPrefix("DHYANA") else { return }
        print("peripheral.name===\(peripheral.name ?? "no name") ---- \(RSSI.intValue)")
        let device = BluetoothDevice(name: peripheralName, connectionState: BluetoothDevice.ConnectionState.getState(forValue: peripheral.state.rawValue), rssi: RSSI.intValue)
        NotificationCenter.default.post(name: .DidDiscoverNewBluetoothDevice, object: device)
        if !peripherals.contains (where: { $0.identifier == peripheral.identifier }) {
            peripherals.append(peripheral)
        }
    }

    func connectToDevice(withName name: String) {
        guard let peripheral = peripherals.filter({ $0.name ?? "No Name" == name }).first else {
            print("Could not find any peripheral named \(name)")
            return
        }
        centralManager.connect(peripheral, options: nil)
        postConnectionStateChangeNotification(forPeripheral: peripheral)
    }

    func disconnectFromDevice(withName name: String) {
        guard let connectedPeripheral = connectedPeripheral
              /*connectedPeripheral.name ?? "No Name" == name*/ else {
                print("No Connected Peripheral named \(name)")
                return
        }
        centralManager.cancelPeripheralConnection(connectedPeripheral)
        postConnectionStateChangeNotification(forPeripheral: connectedPeripheral)
    }

    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {

        print("/////////////////// CONNECTED TO \(peripheral.name ?? "")")
        Ring.shared.connected = true
        connectedPeripheral = peripheral
        connectedPeripheral?.delegate = self
        postConnectionStateChangeNotification(forPeripheral: peripheral)
        if let connectionUpdates = self.continuousDeviceConnectionState { connectionUpdates(true) }
        shouldStopReconnection = false
        guard let peripheralName = peripheral.name else { return }
        UserDefaults.standard.set(peripheralName, forKey: Config.connected_device_name)
        self.connectedPeripheral!.discoverServices(nil)
    }

    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {

        print("/////////////////// FAILED TO CONNECT TO \(peripheral.name ?? "")")
        print("Error : \(error?.localizedDescription ?? "No Description provided")")
        connectedPeripheral = nil
        postConnectionStateChangeNotification(forPeripheral: peripheral)
    }

    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {

        print("/////////////////// DISCONNECTED FROM \(peripheral.name ?? "")")
        if self.connectedPeripheral == peripheral
        {
            Ring.shared.connected = false
            if let connectionUpdates = self.continuousDeviceConnectionState{
                connectionUpdates(false)
            }
            Ring.shared.connected = false
            postConnectionStateChangeNotification(forPeripheral: peripheral)
        }
    }

    func postConnectionStateChangeNotification(forPeripheral peripheral: CBPeripheral) {
        guard let peripheralName = peripheral.name else { return }
        let device = BluetoothDevice(name: peripheralName, connectionState: BluetoothDevice.ConnectionState.getState(forValue: peripheral.state.rawValue))
        NotificationCenter.default.post(name: .DidChangeDeviceConnectionState, object: device)
        for index in 0..<peripherals.count {
            if peripherals[index].name == peripheral.name {
                print("/////////\(peripherals[index].name!) --> \(peripherals[index].state.rawValue)")
            }
        }
    }


    func setSessionTimeStamp(timeStamp: Int)
    {
        let bytesArray = toByteArray(timeStamp)// doubleToByteArray(value: timeStamp)
        var syncByte:[__uint8_t] = [0x11, 0x04]
        syncByte.append(contentsOf: bytesArray)
        var newBytes = syncByte[0...5]
        let syncData = NSData(bytes:&newBytes, length:newBytes.count)
        if logChar != nil {
            self.connectedPeripheral?.writeValue(syncData as Data, for: logChar!, type: .withoutResponse)
        }
    }
    
    typealias Byte = UInt8
    
    func toByteArray<T>(_ value: T) -> [UInt8] {
        var value = value
        return withUnsafeBytes(of: &value) { Array($0) }
    }

    //[0, 0, 0, 0, 0, 0, 52, 64]
    //[0, 0, 0, 0, 0, 0, 52, 64, 92, 111, 0]
    func doubleToByteArray(value: Int) -> [__uint8_t] {
        let count = 4
        let doubles: [Int] = [value]
        let data = NSData(bytes: doubles, length: count)
        var result = [__uint8_t](repeating: 0x00, count: count)
        data.getBytes(&result, length: count)
        return result
    }
    
    func startScanning(for name: String){
        ringName = name
        self.centralManager.scanForPeripherals(withServices: nil, options: nil)
    }

    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        
        if let services = peripheral.services {
            
            for service in services {
                let thisService = service as CBService
                peripheral.discoverCharacteristics(nil, for: thisService)
            }
        } else {
            
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        
        print("service===\(service.uuid)")
        for characteristic in service.characteristics! {
            if(characteristic.uuid == FLIP_BATTERY || characteristic.uuid == FIRMWARE_VERSION_UUID || characteristic.uuid == HEART_RATE_WITH_TIMESTAMP || characteristic.uuid == DEVICE_STATUS_INFORMATION || characteristic.uuid == HARDWARE_VERSION_UUID)
            {
                if characteristic.uuid == HEART_RATE_WITH_TIMESTAMP
                {
                    heartRateCharacteristic = characteristic
                    print("/////////////////// DISCOVERED HEART RATE CHARACTERISTICS FOR \(peripheral.name ?? "")")
                    NotificationCenter.default.post(name: .DidDiscoverHRCharacteristic, object: nil)
                }
                else
                {
                    self.connectedPeripheral!.readValue(for: characteristic)
                    self.connectedPeripheral!.setNotifyValue(true, for: characteristic)
                }
            }
            else if (characteristic.uuid == FLIP_SYSYEM_CMD_UUID) {
                logChar = characteristic
               // let timestamp = Date().timeIntervalSince1970.int
          //      RingConnectionManager.sharedManager.setSessionTimeStamp(timeStamp: timestamp)
              //  RingConnectionManager.sharedManager.setSessionTimeStamp(timeStamp: Int(Date.timeIntervalSince2019))
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
   
        if(characteristic.uuid.isEqual(FLIP_BATTERY) || characteristic.uuid.isEqual(FIRMWARE_VERSION_UUID) || characteristic.uuid.isEqual(HARDWARE_VERSION_UUID) || characteristic.uuid == HEART_RATE_WITH_TIMESTAMP) || characteristic.uuid == DEVICE_STATUS_INFORMATION {
            
            if(characteristic.uuid.isEqual(HEART_RATE_WITH_TIMESTAMP)) {
//                print("//////////////////// HEART RATE ===== \(characteristic.value!)")
                Ring.shared.recievedHeartRate(data: characteristic.value!)
            }
            else if characteristic.uuid.isEqual(FLIP_BATTERY){
                if let data = characteristic.value
                {
                    Ring.shared.recievedBattery(data: data)
                }
                //TODO:- Remove below lines
         //       let timestamp = Date().timeIntervalSince1970.int
//                DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
//                    print("after 5 seconds")
//                    RingConnectionManager.sharedManager.setSessionTimeStamp(timeStamp: 0)
//                }
            }
            else if characteristic.uuid.isEqual(DEVICE_STATUS_INFORMATION){
//                print("////////////////// DEVICE STATUS ===== \(characteristic.value!)")
                Ring.shared.recievedSignalStrength(data: characteristic.value!)
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
        
        if characteristic.uuid.isEqual(FLIP_BATTERY){
            Ring.shared.recievedBattery(data: characteristic.value!)
        }
    }
    
    func getBleManager() -> CBCentralManager{
        return centralManager
    }
    
    func getBlePeripheral() -> CBPeripheral{
        return connectedPeripheral!
    }
    
    func startGettingHeartRates()
    {
        if let hrCharacteristic = heartRateCharacteristic
        {
            connectedPeripheral?.setNotifyValue(true, for: hrCharacteristic)
        }
    }
    
    func stopGettingHeartRates()
    {
        if let hrCharacteristic = heartRateCharacteristic
        {
            if let peripheral = connectedPeripheral
            {
                peripheral.setNotifyValue(false, for: hrCharacteristic)
            }
        }
    }
    
    func unpairDevice(completion: ((Bool)->())? = nil){
        
        centralManager.stopScan()
        shouldStopReconnection = true
        UserDefaults.standard.set(nil, forKey: Config.connected_device_name)
        if connectedPeripheral != nil {
            Ring.shared.connected = false
            centralManager.cancelPeripheralConnection(connectedPeripheral!)
            Ring.shared.resetRingStatus()
            if completion != nil
            {
                completion!(true)
            }
        }
    }
    
    func pairDevice(){
        shouldStopReconnection = false
    }
    
    //   @available(iOS 10.0, *)
    func getBleState() -> CBManagerState {
        return centralManager.state
    }
    
    func reconnect(){
        startBLEConnection()
        if let connectedPeripherel = connectedPeripheral {
            connectedPeripherel.delegate = self
            centralManager.connect(connectedPeripherel, options: nil)
        }
    }
    
    func resetConnection()
    {
        connectedPeripheral = nil
        ringName = nil
    }
    
    func floatValue(data: Data) -> Float {
        return Float(bitPattern: UInt32(bigEndian: data.withUnsafeBytes { $0.pointee } ))
    }
 }
 

