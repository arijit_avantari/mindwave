//
//  RequiredExtensions.swift
//  Mindwave
//
//  Created by Arijit Sarkar on 15/11/19.
//  Copyright © 2019 Avantari. All rights reserved.
//

import UIKit

extension Array where Element: Numeric {
     /// Returns the total sum of all elements in the array
     var total: Element { return reduce(0, +) }
}

extension Array where Element: BinaryFloatingPoint {
     /// Returns the average of all elements in the array
     var average: Int {
          return isEmpty ? 0 : Int(total) / count
     }
}

extension Array where Element: BinaryInteger {
    /// Returns the average of all elements in the array
    var average: Int {
        return isEmpty ? 0 : Int(total) / count
    }

    var floatAverage: Float {
        return isEmpty ? 0.0 : Float(total) / count.float
    }
}

extension Double {
     /// Rounds the double to decimal places value
     func roundTo(places:Int) -> Double {
          let divisor = pow(10.0, Double(places))
          return (self * divisor).rounded() / divisor
     }
}

extension Float {
    /// Rounds the double to decimal places value
    func roundTo(places:Int) -> Float {
        let divisor = pow(10.0, Float(places))
        return (self * divisor).rounded() / divisor
    }

    func isInteger() -> Bool {
        return (floor(self) == self)
    }
}

extension Int
{
    var cgfloat: CGFloat {
        return CGFloat(self)
    }

    var float: Float {
        return Float(self)
    }

    var double: Double {
        return Double(self)
    }

    var string: String {
        return "\(self)"
    }
}

extension Float
{
    var cgfloat: CGFloat {
        return self == .nan ? 0.0 : CGFloat(self)
    }

    var int: Int {
        return self == .nan ? 0 : Int(self)
    }

    var double: Double {
        return self == .nan ? 0.0 : Double(self)
    }

    var string: String {
        return self == .nan ? "" : "\(self)"
    }
}

extension Double
{
    var cgfloat: CGFloat {
        return self == .nan ? 0.0 : CGFloat(self)
    }

    var float: Float {
        return self == .nan ? 0.0 : Float(self)
    }

    var int: Int {
        if self == .nan
        {
            return 0
        }
        else
        {
            return Int(self)
        }
    }

    var string: String {
        return self == .nan ? "" : "\(self)"
    }
}

extension CGFloat
{
    var int: Int {
        return self == .nan ? 0 : Int(self)
    }

    var float: Float {
        return self == .nan ? 0.0 : Float(self)
    }

    var double: Double {
        return self == .nan ? 0.0 : Double(self)
    }

    var string: String {
        return self == .nan ? "" : "\(self)"
    }
}

extension String
{
    var int: Int {
        return Int(self) ?? 0
    }

    var float: Float {
        return Float(self) ?? 0.0
    }

    var double: Double {
        return Double(self) ?? 0.0
    }
}

extension Data
{
    func battery() -> Int
    {
        let batteryData = self as NSData?
        var values = [UInt64](repeating:0, count:(batteryData?.length)!)
        batteryData?.getBytes(&values, length: (batteryData?.length)!)
        return Int((batteryData?.u8)!)
    }

    func heartRate() -> (Double, Double)?
    {
        let heartRateData = self as NSData?
        var dataArray = [UInt8](repeating:0, count: (heartRateData?.length)!)
        heartRateData?.getBytes(&dataArray, length: (heartRateData?.length)!)
        if dataArray.count >= 12{

            let hrByteArray = dataArray[0...3]
            let timeStampByteArray = dataArray[4...11]
            let hrData:NSData = Data(hrByteArray) as NSData;
            let timeStampData:NSData = Data(timeStampByteArray) as NSData;
            let hr = Double(hrData.float32)
            let ts = Double(timeStampData.u64)
            return (hr, ts)
        }
        return nil
    }

    func ringStatus() -> (fingerDetection: Bool, signalStrength: Int, stability: Int, isCharging: Bool)
    {
        let signalData:NSData? = self as NSData?

        var dataArray = [UInt8] (repeating:0, count: (signalData?.length)!)
        signalData?.getBytes(&dataArray, length: (signalData?.length)!)
        if dataArray.count > 1 {
            if let value = dataArray.first
            {
                if value == 3
                {
                    let strength = Int(dataArray[1])
                    let stability = Int(dataArray[2])
                    return (true, strength, stability, false)
                }
                else if value == 6
                {
                    return (false, 0, 0, true)
                }
            }
        }
        return (false, 0, 0, false)
    }

    func getHexString() -> String {

        let signalData:NSData? = self as NSData?
        var description = signalData!.description
        description.removeFirst()
        description.removeLast()
        description = description.replacingOccurrences(of: " ", with: "")
        description = description.uppercased()
        return description
    }
}

extension NSData {
     var u8:UInt8 {
          assert(self.length >= 1)
          var byte:UInt8 = 0x00
          self.getBytes(&byte, length: 1)
          return byte
     }
     var u16:UInt16 {
          assert(self.length >= 2)
          var word:UInt16 = 0x0000
          self.getBytes(&word, length: 2)
          return word
     }

     var u32:UInt32 {
          assert(self.length >= 4)
          var u32:UInt32 = 0x00000000
          self.getBytes(&u32, length: 4)
          return u32
     }

     var u64:UInt64{
          assert(self.length >= 8)
          var u64:UInt64 =  0x00000000000000000
          self.getBytes(&u64, length: 8)
          return u64

     }

     var float32:Float32 {
          //0x2A
          //0x3fc00000
          assert(self.length >= 4)
          var float32:Float32 = 0x3fc00000
          self.getBytes(&float32, length: 4)
          return float32
     }
     var u8s:[UInt8] { // Array of UInt8, Swift byte array basically
          var buffer:[UInt8] = [UInt8](repeating: 0, count: self.length)
          self.getBytes(&buffer, length: self.length)
          return buffer
     }

     var utf8:String? {
          return String(data: self as Data, encoding: String.Encoding.utf8)
     }
}

extension UIColor {

    static var dhyanaBlue: UIColor  {
        return UIColor.color(red: 82, green: 195, blue: 237)  //100 190 251 //130 209 250
    }

    static func color(red: Int, green: Int, blue: Int) -> UIColor {
        return UIColor.color(red: red, green: green, blue: blue, alpha: 1.0)
    }

    static func color(red: Int, green: Int, blue: Int, alpha: Double) -> UIColor {
        return UIColor(red: CGFloat(Double(red)/255.0), green: CGFloat(Double(green)/255.0), blue: CGFloat(Double(blue)/255.0), alpha: CGFloat(alpha))
    }

}
