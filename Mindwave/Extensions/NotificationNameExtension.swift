//
//  NotificationNameExtension.swift
//  Mindwave
//
//  Created by Arijit Sarkar on 15/11/19.
//  Copyright © 2019 Avantari. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let DidFinishedSession = Notification.Name("DidFinishedSession")
    static let DidUpdatedBatteryPercentage = Notification.Name("DidUpdatedBatteryPercentage")
    static let DidUpdatedChargingState = Notification.Name("DidUpdatedChargingState")
    static let DidUpdatedSignalStrength = Notification.Name("DidUpdatedSignalStrength")
    static let DidChangeDeviceConnectionState = Notification.Name("DidChangedDeviceConnectionState")
    static let DidRemoveRingFromFinger = Notification.Name("DidRemoveRingFromFinger")
    static let DidChangeBlueToothState = Notification.Name("DidChangedBlueToothState")
    static let DidDiscoverNewBluetoothDevice = Notification.Name("DidChangedBlueToothState")
    static let DidDiscoverHRCharacteristic = Notification.Name("DidDiscoverHRCharacteristic")
}

