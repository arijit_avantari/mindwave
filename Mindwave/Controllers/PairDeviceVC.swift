//
//  PairDeviceVC.swift
//  Mindwave
//
//  Created by Arijit Sarkar on 15/11/19.
//  Copyright © 2019 Avantari. All rights reserved.
//

import UIKit
import CoreBluetooth

struct BluetoothDevice {
    enum ConnectionState: Int {
        case disconnected, connecting, connected, disconnecting
        var text: String {
            switch self {
            case .connected: return "Connected"
            case .disconnected: return "Not Connected"
            case .connecting: return "Connecting"
            case .disconnecting: return "Disconnecting"
            }
        }
        static func getState(forValue: Int) -> ConnectionState {
            return ConnectionState(rawValue: forValue)!
        }
    }
    var name: String
    var connectionState: ConnectionState = .disconnected
    var rssi: Int = -100

    mutating func updateRSSI(to: Int) {
        rssi = to
    }
}

class PairDeviceVC: UIViewController {

    var connectedDevice: BluetoothDevice? {
        didSet {
            updateDeviceDetails()
        }
    }
    var bluetoothDevices = [BluetoothDevice]()
    var sessions = [SessionData]() {
        didSet {
            totalSessionCountLabel.text = "\(sessions.count)"
        }
    }
    enum DeviceStatus {
        case active, inactive
        var desc: String {
            switch self {
            case .active: return "Status : Active"
            case .inactive: return "Status : Inactive"
            }
        }
    }
    var sessionTableViewDelegate: SessionsTableViewDelegate?

    @IBOutlet weak var bluetoothStatusIndicatorView: UIView!
    @IBOutlet weak var bluetoothDeviceTableView: UITableView!
    @IBOutlet weak var sessionTableView: UITableView!
    @IBOutlet weak var goToSessionPageButton: UIButton!
    @IBOutlet weak var deviceParametersView: UIView!
    @IBOutlet weak var sessionLogsView: UIView!
    @IBOutlet weak var deviceListView: UIView!
    @IBOutlet weak var deviceStatusLabel: UILabel!
    @IBOutlet weak var deviceConnectAtLabel: UILabel!
    @IBOutlet weak var batteryPercentageLabel: UILabel!
    @IBOutlet weak var signalStrengthLabel: UILabel!
    @IBOutlet weak var leafyBackgroundView: LeafyBackgroundView!
    @IBOutlet weak var totalSessionCountLabel: UILabel!
    @IBOutlet weak var bluetoothHeaderView: UIView!
    @IBOutlet weak var sessionsHeaderView: UIView!
    @IBOutlet weak var settingsButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        fetchSessionData()
        leafyBackgroundView.beginAnimations()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        leafyBackgroundView.endAnimations()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    @IBAction func goToSessionPageButtonTapped(_ sender: UIButton) {
        RingConnectionManager.sharedManager.centralManager.stopScan()
        guard let sessionVC = UIStoryboard(name: "Main", bundle: .main).instantiateViewController(withIdentifier: "SessionVC") as? SessionVC else { return }
        self.navigationController?.pushViewController(sessionVC, animated: true)
    }

    @IBAction func settingsButtonTapped(_ sender: UIButton) {
        guard let settingsVC = UIStoryboard(name: "Main", bundle: .main).instantiateViewController(withIdentifier: "SettingsVC") as? SettingsVC else { return }
        self.navigationController?.pushViewController(settingsVC, animated: true)
    }

    func initialSetup() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateBluetoothStatus(_:)), name: .DidChangeBlueToothState, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateBluetoothDeviceList(_:)), name: .DidDiscoverNewBluetoothDevice, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateDeviceConnectionStatus(_:)), name: .DidChangeDeviceConnectionState, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(batteryPercentageUpdate(_:)), name: .DidUpdatedBatteryPercentage, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(signalStrengthUpdate(_:)), name: .DidUpdatedSignalStrength, object: nil)
        RingConnectionManager.sharedManager.startBLEConnection()
        connectedDevice = nil
        bluetoothDeviceTableView.dataSource = self
        bluetoothDeviceTableView.delegate = self
        let view1 = UIView()
        bluetoothDeviceTableView.tableFooterView = view1
        sessionTableView.dataSource = self
        sessionTableViewDelegate = SessionsTableViewDelegate(tableView: sessionTableView, shadowView: sessionsHeaderView)
        sessionTableView.delegate = sessionTableViewDelegate
        sessionTableView.allowsSelection = false
        let view2 = UIView()
        sessionTableView.tableFooterView = view2
    }

    func setupUI() {
        bluetoothStatusIndicatorView.backgroundColor = RingConnectionManager.sharedManager.bluetoothConnected ? .green : .red
        bluetoothStatusIndicatorView.layer.cornerRadius = 10
        bluetoothStatusIndicatorView.layer.masksToBounds = true
        [deviceListView, deviceParametersView, sessionLogsView, goToSessionPageButton].forEach {
            $0?.layer.cornerRadius = 5
            $0?.layer.masksToBounds = true
        }
        enableDisableStartButton(shouldEnable: connectedDevice != nil)
        [bluetoothHeaderView, sessionsHeaderView].forEach {
            $0?.layer.shadowOffset = CGSize(width: 0, height: 2)
            $0?.layer.shadowColor = UIColor.black.cgColor
            $0?.layer.shadowOpacity = 0.0
            $0?.layer.shadowRadius = 2
        }
    }

    func enableDisableStartButton(shouldEnable: Bool) {
        goToSessionPageButton.isEnabled = shouldEnable
        goToSessionPageButton.alpha = shouldEnable ? 1.0 : 0.3
    }

    @objc func updateBluetoothStatus(_ notification: Notification) {
        let isBluetoothOn = RingConnectionManager.sharedManager.bluetoothConnected
        bluetoothStatusIndicatorView.backgroundColor = isBluetoothOn ? .green : .red
        if !isBluetoothOn {
            connectedDevice = nil
            bluetoothDevices.removeAll()
            bluetoothDeviceTableView.reloadData()
        } else {
            RingConnectionManager.sharedManager.centralManager.scanForPeripherals(withServices: nil, options: nil)
        }
    }

    @objc func updateBluetoothDeviceList(_ notification: Notification) {
        guard let device = notification.object as? BluetoothDevice else { return }
        let indexOfDevice = bluetoothDevices.firstIndex (where: { $0.name == device.name })
        if indexOfDevice == nil {
            bluetoothDevices.append(device)
        } else {
            bluetoothDevices[indexOfDevice!].updateRSSI(to: device.rssi)
        }
//        bluetoothDevices = bluetoothDevices.sorted { $0.rssi > $1.rssi }
        bluetoothDeviceTableView.reloadData()
    }

    @objc func updateDeviceConnectionStatus(_ notification: Notification) {
        guard let device = notification.object as? BluetoothDevice else { return }
        for (index,item) in bluetoothDevices.enumerated() {
            if item.name == device.name {
                bluetoothDevices[index] = device
                bluetoothDeviceTableView.beginUpdates()
                bluetoothDeviceTableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .fade)
                bluetoothDeviceTableView.endUpdates()
            }
        }
        connectedDevice = bluetoothDevices.filter { $0.connectionState == .connected }.first
        bluetoothDeviceTableView.reloadData()
    }

    func updateDeviceDetails() {
        if connectedDevice != nil {
            deviceStatusLabel.text = DeviceStatus.active.desc
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yy, hh:mm a"
            let date = formatter.string(from: Date())
            deviceConnectAtLabel.text = "Connected : \(date)"
            enableDisableStartButton(shouldEnable: true)
        } else {
            deviceStatusLabel.text = DeviceStatus.inactive.desc
            deviceConnectAtLabel.text = ""
            batteryPercentageLabel.text = ""
            signalStrengthLabel.text = ""
            enableDisableStartButton(shouldEnable: false)
        }
    }

    func fetchSessionData() {
        sessions = SessionStorage.getAllSessions()
        sessionTableView.reloadData()
    }
    
}

extension PairDeviceVC {

    @objc func batteryPercentageUpdate(_ notification: Notification) {
        if let percentage = notification.object as? Int {
            batteryPercentageLabel.text = "Battery : \(percentage) %"
        }
    }

    @objc func signalStrengthUpdate(_ notification: Notification) {
        if let signal = notification.object as? Signal {
            signalStrengthLabel.text = "Signal : " + min((signal.averageSignalPercentage.float * (10.0 / 7.0)).int, 100).string + "%"
        }
    }
}

extension PairDeviceVC: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableView.tag == 1 ? bluetoothDevices.count : sessions.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: BluetoothDeviceTVC.identifier, for: indexPath) as! BluetoothDeviceTVC
            cell.deviceNameLabel.text = bluetoothDevices[indexPath.row].name
            cell.connectionState = bluetoothDevices[indexPath.row].connectionState
            cell.rssiLabel.text = "\(abs(bluetoothDevices[indexPath.row].rssi))"
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: SessionDataTVC.identifier, for: indexPath) as! SessionDataTVC
            let timestamp = sessions[indexPath.row].timestamp
            let date = Date(timeIntervalSince1970: Double(timestamp))
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MMM-yy, hh:mm:ss a"
            cell.timeLabel.text = formatter.string(from: date)
            let val = sessions[indexPath.row].performance
            let score = sessions[indexPath.row].score
            let performance = SessionVC.SessionPerformance(rawValue: val)!
            cell.performanceLabel.text = performance.text
            cell.performanceLabel.textColor = performance.indicatorColor
            cell.scoreLabel.text = score == -1 ? "NA" : String(format: "%.2f", score)
            return cell
        }
    }
}

extension PairDeviceVC: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard tableView.tag == 1 else { return }
        if bluetoothDevices[indexPath.row].connectionState == .connected {
            RingConnectionManager.sharedManager.disconnectFromDevice(withName: bluetoothDevices[indexPath.row].name)
        } else if bluetoothDevices[indexPath.row].connectionState == .disconnected {
            if connectedDevice != nil {
                RingConnectionManager.sharedManager.disconnectFromDevice(withName: connectedDevice!.name)
            }
            RingConnectionManager.sharedManager.connectToDevice(withName: bluetoothDevices[indexPath.row].name)
        } else {
             
        }
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y > 0 {
            bluetoothHeaderView.layer.shadowOpacity = min(Float(scrollView.contentOffset.y * 0.004 * 0.3),0.3)
        } else {
            bluetoothHeaderView.layer.shadowOpacity = 0.0
        }
    }
}

class SessionsTableViewDelegate: NSObject, UITableViewDelegate {

    weak var tableView: UITableView!
    weak var shadowView: UIView!

    init(tableView: UITableView, shadowView: UIView) {
        self.tableView = tableView
        self.shadowView = shadowView
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y > 0 {
            shadowView.layer.shadowOpacity = min(Float(scrollView.contentOffset.y * 0.004 * 0.3),0.3)
        } else {
            shadowView.layer.shadowOpacity = 0.0
        }
    }

}
