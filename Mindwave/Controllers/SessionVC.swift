//
//  SessionVC.swift
//  Mindwave
//
//  Created by Arijit Sarkar on 15/11/19.
//  Copyright © 2019 Avantari. All rights reserved.
//

import UIKit

class SessionTimer {

    private var timer = Timer()
    private var stepTimeInterval: TimeInterval!
    private var finalTimeInterval: TimeInterval!
    private var onTimerUpdate: ((TimeInterval) -> Void)!
    private var timePassed: TimeInterval = 0

    init(stepTimeInterval: TimeInterval, finalTimeInterval: TimeInterval, onTimerUpdate: @escaping ((TimeInterval) -> Void)) {
        self.stepTimeInterval = stepTimeInterval
        self.finalTimeInterval = finalTimeInterval
        self.onTimerUpdate = onTimerUpdate
    }

    func startTimer() {
        timer = Timer.scheduledTimer(withTimeInterval: stepTimeInterval, repeats: true, block: { (_) in

            if self.timePassed == self.finalTimeInterval {
                self.stopTimer()
            }
            self.onTimerUpdate(self.timePassed)
            self.timePassed += self.stepTimeInterval
        })
    }

    func stopTimer() {
        timer.invalidate()
    }

}

class SessionVC: UIViewController {

    //MARK: IBOutlet
    @IBOutlet weak var topInstructionLabel: UILabel!
    @IBOutlet weak var bottomInstructionLabel: UILabel!
    @IBOutlet weak var sineWaveView: DhyanaSineWaveView!
    @IBOutlet weak var dhyanaTitleStackView: UIStackView!
    @IBOutlet weak var dhyanaTitleViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var inhaleExhaleView: InOutLine!
    @IBOutlet weak var sessionResultLabel: UILabel!
    @IBOutlet weak var contactInfoLabel: UILabel!
    @IBOutlet weak var websiteUrlLabel: UILabel!
    @IBOutlet weak var leafyBackgroundView: LeafyBackgroundView!
    @IBOutlet var backTapGesture: UITapGestureRecognizer!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var hrLabel: UILabel!

    //MARK: Properties
    var currentSignal: Signal?
    var firstTime = true
    var startTimestamp: Double = 0.0
    var hasHRCharacteristicsBeenDiscovered = false {
        didSet {
            self.bottomInstructionLabel.text = hasHRCharacteristicsBeenDiscovered ? "Place your hand on the sensor to start." : "Discovering Heart Rate Characteristics..."
        }
    }
    var sessionterminatedDueToInterruption = false
    var isWaitingBeforeNewSession = false
    var isRingSignalLow = false
    var isSessionPaused = false
    var inhaleExhaleDuration: TimeInterval = 6
    var sessionStartDuration: TimeInterval = 3
    var totalSessionDuration: TimeInterval = 30
    var showResultDuration: TimeInterval = 3
    var waitingBetweenSessionsDuration: TimeInterval = 3
    var sessionTerminationDueToInterruptionDuration: TimeInterval = 10
    var lastPausedSessionTime: TimeInterval = 0
    var currentSessionTimePassed: TimeInterval = 0 {
        didSet {
            print("------------------------- SESSION TIMER : \(currentSessionTimePassed) ------------------------------")
        }
    }
    var lastPausedInhaleExhaleTime: TimeInterval = 0
    var currentInhaleExhaleTimePassed: TimeInterval = 0 {
        didSet {
            print("------------------------- INHALE EXHALE TIMER : \(currentSessionTimePassed) ------------------------------")
        }
    }
    let maxAllowedVariance = CGFloat(1.3)
    var idleStateHRVarianceValue = CGFloat(1.3)
    var ongoingStateNoSignalHRVarianceValue = CGFloat(0.4)
    var dhyanaTitleInitialTopConstant = CGFloat(110)
    var dhyanaTitleFinalTopConstant = CGFloat(20)
    var hrVarianceToAnalyze = [Double]()

    var currentSessionStage = SessionStage.idle
    var inhaleExhaleTimer: SessionTimer?
    var prepareSessionTimer: SessionTimer?
    var sessionTimer: SessionTimer?
    var showResultTimer: SessionTimer?
    var timeBetweenSessionTimer: SessionTimer?
    var sessionTerminationDueToInterruptionTimer: SessionTimer?

    enum SessionStage {
        case idle, preparing, ongoing, ended, problematic
    }

    //MARK: Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        hasHRCharacteristicsBeenDiscovered = true
        heartRateCharacteristicsDiscovered()
        Ring.startGettingHeartRates()
        inhaleExhaleView.setupPaths()
        setupUI(forStage: .idle)
    }

    //MARK: IBAction
    @IBAction func didTapOnBack(_ sender: UITapGestureRecognizer) {
        releaseResources()
        self.navigationController?.popViewController(animated: true)
    }

    func releaseResources() {
        Ring.shared.hrDelegate = nil
        Ring.shared.signalDelegate = nil
        inhaleExhaleTimer?.stopTimer()
        prepareSessionTimer?.stopTimer()
        sessionTimer?.stopTimer()
        timeBetweenSessionTimer?.stopTimer()
        showResultTimer?.stopTimer()
        sessionTerminationDueToInterruptionTimer?.stopTimer()
        NotificationCenter.default.removeObserver(self)
    }

    //MARK: Helper Methods
    private func initialSetup() {
        NotificationCenter.default.addObserver(self, selector: #selector(heartRateCharacteristicsDiscovered), name: .DidDiscoverHRCharacteristic, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(signalStrengthChanged(_:)), name: .DidUpdatedSignalStrength, object: nil)
        dhyanaTitleStackView.alpha = 0.0
        topInstructionLabel.alpha = 0.0
        bottomInstructionLabel.alpha = 0.0
        leafyBackgroundView.alpha = 0.0
        SessionConfiguration.shared.description()
    }

    @objc func heartRateCharacteristicsDiscovered() {
        hasHRCharacteristicsBeenDiscovered = true
        Ring.shared.hrDelegate = self
        Ring.shared.signalDelegate = self
        Ring.startGettingHeartRates()
    }

    @objc func signalStrengthChanged(_ notification: Notification) {
        print(currentSessionStage)
        guard let signal = notification.object as? Signal else { return }
        currentSignal = signal
        guard currentSessionStage == .ongoing else { return }
        if !signal.fingerDetected {
            print("%%%%%%%%%%%%%%%%%%%%%%%%SignalStrength=========\(signal.signalStrength)")
            isRingSignalLow = true
            bottomInstructionLabel.textColor = AppColor.inactiveColor
            updateBottomInstructionLabel(to: AppConstants.placeYourFingerBack)
            if !isSessionPaused {
                isSessionPaused = true
                print("::::::::::::::: PAUSE ::::::::::::::::::::\n::::::::::::::: PAUSE ::::::::::::::::::::\n::::::::::::::: PAUSE ::::::::::::::::::::")
                pauseSession()
            }
        } else {
            if isRingSignalLow && isSessionPaused {
                print("%%%%%%%%%%%%%%%%%%%%%%%%SignalStrength=========\(signal.signalStrength)")
                print("::::::::::::::: RESUME ::::::::::::::::::::\n::::::::::::::: RESUME ::::::::::::::::::::\n::::::::::::::: RESUME ::::::::::::::::::::")
                bottomInstructionLabel.textColor = AppColor.activeColor
                updateBottomInstructionLabel(to: "")
                resumeSession()
                isRingSignalLow = false
                isSessionPaused = false
            }
        }
    }

    //MARK: UI Setup
    private func setupUI(forStage stage: SessionStage) {
        switch stage {
        case .idle: setupUIForIdleStage()
        case .preparing: setupUIForPreparingSessionStage()
        case .ongoing: setupUIForOngoingStage()
        case .problematic: break
        case .ended: setupUIForEndedStage()
        }
    }

    private func setupUIForIdleStage() {
        sessionResultLabel.hideWithAnimation(duration: 0.3) {

            if self.currentSessionStage == .idle {
                self.dhyanaTitleViewTopConstraint.constant = self.dhyanaTitleInitialTopConstant
            } else {
                self.dhyanaTitleViewTopConstraint.constant = self.dhyanaTitleFinalTopConstant
            }
            UIView.animate(withDuration: 0.3, animations: {
                self.dhyanaTitleStackView.superview?.layoutIfNeeded()
            }, completion: nil)

            self.topInstructionLabel.text = AppConstants.calmWaveCalmMindText
            self.bottomInstructionLabel.textColor = AppColor.activeColor
            self.bottomInstructionLabel.text = self.hasHRCharacteristicsBeenDiscovered ? AppConstants.placeYourHandToStartText : AppConstants.discoveringHeartRateCharacteristicsText
            self.topInstructionLabel.showWithAnimation(duration: 0.5, havingCompletion: nil)
            self.bottomInstructionLabel.showWithAnimation(duration: 0.7, havingCompletion: nil)

            if self.firstTime {
                self.firstTime = false
                self.leafyBackgroundView.showWithAnimation(duration: 0.2, havingCompletion: {
                    self.leafyBackgroundView.beginAnimations()
                })
                self.dhyanaTitleStackView.showWithAnimation(duration: 0.3, havingCompletion: nil)
            } else {
                Ring.startGettingHeartRates()
                self.waitBeforeStartingNewSession()
            }

            self.sineWaveView.waveColor = AppColor.activeColor
            self.sineWaveView.showWithAnimation(duration: 0.4, havingCompletion: {
                self.sineWaveView.updateWithLevel(level: self.idleStateHRVarianceValue)
                self.sineWaveView.startAnimating()
            })
        }
    }

    private func setupUIForPreparingSessionStage() {
        startTimestamp = Date().timeIntervalSince1970
        sineWaveView.waveColor = AppColor.inactiveColor
        leafyBackgroundView.hideWithAnimation(duration: 0.2, havingCompletion: {
            self.leafyBackgroundView.endAnimations()
        })
        topInstructionLabel.hideWithAnimation(duration: 0.3, havingCompletion: nil)
        bottomInstructionLabel.hideWithAnimation(duration: 0.3, havingCompletion: nil)
        self.bottomInstructionLabel.text = ""
        self.bottomInstructionLabel.showWithAnimation(duration: 0.3, havingCompletion: nil)
        self.dhyanaTitleViewTopConstraint.constant = self.currentSessionStage == .idle ? CGFloat(116) : CGFloat(19)
        UIView.animate(withDuration: 0.3, animations: {
            self.dhyanaTitleStackView.superview?.layoutIfNeeded()
        }, completion: nil)
        prepareSessionTimer = SessionTimer(stepTimeInterval: 1, finalTimeInterval: sessionStartDuration, onTimerUpdate: { [weak self] (passedTime) in
            guard let welf = self else { return }
            if passedTime >= welf.sessionStartDuration {
                welf.startSession()
                welf.prepareSessionTimer?.stopTimer()
                welf.prepareSessionTimer = nil
            } else {
                DispatchQueue.main.async {
                    if !(welf.currentSignal?.fingerDetected ?? false) {
                        welf.topInstructionLabel.text = AppConstants.placeYourFingerBack
                        welf.topInstructionLabel.textColor = AppColor.inactiveColor
                        welf.bottomInstructionLabel.textColor = AppColor.inactiveColor
                    } else {
                        welf.topInstructionLabel.text = ""
                        welf.topInstructionLabel.textColor = AppColor.activeColor
                        welf.bottomInstructionLabel.textColor = AppColor.activeColor
                    }
                    welf.bottomInstructionLabel.text = "New Session starting in \(Int(welf.sessionStartDuration - passedTime)) seconds"
                }
            }
        })
        prepareSessionTimer?.startTimer()
    }

    private func setupUIForOngoingStage() {
        topInstructionLabel.hideWithAnimation(duration: 0.3, havingCompletion: nil)
        bottomInstructionLabel.hideWithAnimation(duration: 0.3, havingCompletion: nil)
        if SessionConfiguration.shared.shouldDisplayHRDuringSession {
            hrLabel.showWithAnimation(duration: 0.3, havingCompletion: nil)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.topInstructionLabel.hideWithAnimation(duration: 0.3) {
                self.bottomInstructionLabel.text = "Focus on your breathing."
                self.inhaleExhaleView.showWithAnimation(duration: 0.3) {
                    self.animateInhaleExhaleView()
                }
                self.bottomInstructionLabel.showWithAnimation(duration: 0.3, havingCompletion: nil)
            }
        }
    }

    private func setupUIForEndedStage() {
//        inhaleExhaleView.hideWithAnimation(duration: 0.2, havingCompletion: nil)
        leafyBackgroundView.showWithAnimation(duration: 0.2, havingCompletion: {
            self.leafyBackgroundView.beginAnimations()
        })
        topInstructionLabel.hideWithAnimation(duration: 0.3, havingCompletion: nil)
        bottomInstructionLabel.hideWithAnimation(duration: 0.3, havingCompletion: nil)
        hrLabel.hideWithAnimation(duration: 0.3, havingCompletion: nil)
        sineWaveView.stopAnimating()
        sineWaveView.endAnimations()
        sineWaveView.updateWithLevel(level: idleStateHRVarianceValue)
        self.sineWaveView.waveColor = AppColor.activeColor
        sineWaveView.hideWithAnimation(duration: 0.3) {
            self.displaySessionResult()
            self.sineWaveView.waveColor = AppColor.activeColor
        }
    }

    //MARK: Waiting before every session
    func waitBeforeStartingNewSession() {
        print("///////// WAITING //////////////")
        isWaitingBeforeNewSession = true
        timeBetweenSessionTimer = SessionTimer(stepTimeInterval: 1, finalTimeInterval: waitingBetweenSessionsDuration, onTimerUpdate: { [weak self] (passedTime) in
            if passedTime == self?.waitingBetweenSessionsDuration {
                self?.isWaitingBeforeNewSession = false
                self?.timeBetweenSessionTimer?.stopTimer()
                self?.timeBetweenSessionTimer = nil
            }
        })
        timeBetweenSessionTimer?.startTimer()
    }

    var finishedBreathing = false
    //MARK: Session Controlling Methods
    private func startSession() {
        print("///////// START SESSION //////////////")
        topInstructionLabel.text = ""
        bottomInstructionLabel.text = ""
        topInstructionLabel.textColor = AppColor.activeColor
        bottomInstructionLabel.textColor = AppColor.activeColor
        inhaleExhaleView.setupPaths()
        sineWaveView.waveColor = AppColor.activeColor
        currentSessionStage = .ongoing
        setupUI(forStage: .ongoing)
        sessionTimer = SessionTimer(stepTimeInterval: 1, finalTimeInterval: totalSessionDuration, onTimerUpdate: { [weak self] (passedTime) in
            guard let welf = self else { return }
            welf.currentSessionTimePassed = passedTime
            if passedTime > 10 && passedTime < 27 {
                welf.bottomInstructionLabel.text = AppConstants.calmYourMind
            }
            if passedTime >= welf.totalSessionDuration {
                welf.endSession()
            }
        })
        sessionTimer?.startTimer()
    }

    private func resumeSession() {
        print("///////// RESUME SESSION //////////////")
        guard currentSessionStage == .ongoing else { return }
        sessionTimer?.stopTimer()
        sessionTimer = nil
        sessionTerminationDueToInterruptionTimer?.stopTimer()
        sessionTerminationDueToInterruptionTimer = nil
        animateInhaleExhaleView()
        sineWaveView.waveColor = AppColor.activeColor
        let timeLeft = totalSessionDuration - currentSessionTimePassed
        if timeLeft > 3 {
            sessionTimer = SessionTimer(stepTimeInterval: 1, finalTimeInterval: timeLeft, onTimerUpdate: { [weak self] (passedTime) in
                guard let welf = self else { return }
                welf.currentSessionTimePassed = welf.lastPausedSessionTime + passedTime
                if welf.currentSessionTimePassed > 10 && passedTime < 27 {
                    welf.bottomInstructionLabel.text = AppConstants.calmYourMind
                }
                if passedTime >= timeLeft {
                    welf.endSession()
                }
            })
            sessionTimer?.startTimer()
        } else {
            endSession()
        }
    }

    private func pauseSession() {
        print("///////// PAUSE SESSION //////////////")
        guard currentSessionStage == .ongoing else { return }
        lastPausedSessionTime = currentSessionTimePassed
        lastPausedInhaleExhaleTime = currentInhaleExhaleTimePassed
        sessionTimer?.stopTimer()
        sessionTimer = nil
        inhaleExhaleView.pauseAnimations()
        sineWaveView.waveColor = AppColor.inactiveColor
        sineWaveView.updateWithLevel(level: 0.0) // Base Level
        sessionTerminationDueToInterruptionTimer?.stopTimer()
        sessionTerminationDueToInterruptionTimer = nil
        sessionTerminationDueToInterruptionTimer = SessionTimer(stepTimeInterval: 1, finalTimeInterval: sessionTerminationDueToInterruptionDuration, onTimerUpdate: { [weak self] (passedTime) in
            guard let welf = self else { return }
            if passedTime >= welf.sessionTerminationDueToInterruptionDuration {

                // End the session and return to Idle state
                print("//////////////// TERMINATE //////////////")
                print("//////////////// TERMINATE //////////////")
                print("//////////////// TERMINATE //////////////")
                welf.sessionterminatedDueToInterruption = true
                welf.endSession()
            }
        })
        sessionTerminationDueToInterruptionTimer?.startTimer()
    }

    private func endSession() {
        print("///////// END SESSION //////////////")
        guard currentSessionStage == .ongoing else { return }
        currentSessionStage = .ended
        setupUI(forStage: .ended)
        Ring.stopGettingHeartRates()
        self.inhaleExhaleView.hideWithAnimation(duration: 0.3, havingCompletion: {
            self.inhaleExhaleView.removeAllAnimations()
        })
        self.sessionTimer?.stopTimer()
        self.sessionTimer = nil
        self.sessionTerminationDueToInterruptionTimer?.stopTimer()
        self.sessionTerminationDueToInterruptionTimer = nil
        self.finishedBreathing = true
        self.inhaleExhaleTimer?.stopTimer()
        self.inhaleExhaleTimer = nil
        self.currentSessionTimePassed = 0
        self.currentInhaleExhaleTimePassed = 0
        self.lastPausedInhaleExhaleTime = 0
        self.didStartBreathingAnimation = false
        self.isRingSignalLow = false
        self.isSessionPaused = false
        showResultTimer = SessionTimer(stepTimeInterval: 1, finalTimeInterval: showResultDuration, onTimerUpdate: { [weak self] (passedTime) in
            guard let welf = self else { return }
            if passedTime < welf.showResultDuration {
                // Do Nothing
            } else {
                welf.hideSessionResult()
                welf.showResultTimer?.stopTimer()
                welf.showResultTimer = nil
            }
        })
        showResultTimer?.startTimer()
    }

    //MARK: Inhale Exhale Controlling View
    var didStartBreathingAnimation = false
    func animateInhaleExhaleView() {
        inhaleExhaleTimer?.stopTimer()
        inhaleExhaleTimer = nil
        inhaleExhaleTimer = SessionTimer(stepTimeInterval: 1, finalTimeInterval: inhaleExhaleDuration, onTimerUpdate: { [weak self] (passedTime) in
            guard let welf = self else { return }
            welf.currentInhaleExhaleTimePassed = welf.lastPausedInhaleExhaleTime + passedTime
            if welf.currentInhaleExhaleTimePassed >= welf.inhaleExhaleDuration {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    welf.bottomInstructionLabel.text = ""
                }
                welf.inhaleExhaleTimer?.stopTimer()
                welf.inhaleExhaleTimer = nil
            }
        })
        inhaleExhaleTimer?.startTimer()
        print("///////// INHALE EXHALE //////////////")
        print("========= Last Time Paused ========== \(lastPausedInhaleExhaleTime)")
        if !didStartBreathingAnimation {
            didStartBreathingAnimation = true
            print("========= STARTING ==========")
            print("====filling circle====")
            self.inhaleExhaleView.fillCirlce(duration: 3) {
                print("====unfilling circle====")
                self.inhaleExhaleView.unfillCirlce(duration: 3) {
                    print("====finished====")
                    self.inhaleExhaleView.hideWithAnimation(duration: 0.3, havingCompletion: nil)
                }
            }
        } else {
            print("========= RESUMING ==========")
            self.inhaleExhaleView.resumeAnimations()
        }
    }

    //MARK: Sine Wave Controlling Method
    func getNormalizedVariance(forVriance variance: Double) -> CGFloat {
        let timeFactor: CGFloat = SessionConfiguration.shared.timeWeightageInResult.cgfloat
        let varianceFactor: CGFloat = CGFloat(1.0) - SessionConfiguration.shared.timeWeightageInResult.cgfloat
        let normalizedTimeVariance = timeFactor * maxAllowedVariance * CGFloat((totalSessionDuration - currentSessionTimePassed)/totalSessionDuration)
        let normalizedHRVariance = min(CGFloat(variance),maxAllowedVariance) * varianceFactor
        return (normalizedTimeVariance + normalizedHRVariance)
    }

    func updateSineWave(for heartRate: Double) {
        hrLabel.text = heartRate.int.string
        let variance = HVariance.sharedHVariance.getHeartRateVariance(from: heartRate)
        hrVarianceToAnalyze.append(variance)
        let normalizedvariance = getNormalizedVariance(forVriance: variance)
        print("/////Variance===========\(variance)\n/////Normalized============\(normalizedvariance)\n/////HeartRate=============\(heartRate)")
        sineWaveView.updateWithLevel(level: normalizedvariance, baseLevel: CGFloat(0.0))
    }

    //MARK: Instruction Label
    func updateBottomInstructionLabel(to: String) {
        if bottomInstructionLabel.alpha == 1.0 {
            bottomInstructionLabel.text = to
        } else {
            bottomInstructionLabel.hideWithAnimation(duration: 0.1, havingCompletion: nil)
            bottomInstructionLabel.showWithAnimation(duration: 0.1) {
                self.bottomInstructionLabel.text = to
            }
        }
    }

    //MARK: Display Session Results
    func displaySessionResult() {
        print("///////// DISPLAY RESULT //////////////")
        if sessionterminatedDueToInterruption {
            sessionterminatedDueToInterruption = false
            sessionResultLabel.text = AppConstants.interruptesSessionText
        } else {
            let performance = getSessionPerformance(forHRVariance: hrVarianceToAnalyze)
            let score = performance.0
            let label = performance.1
            DispatchQueue.main.async {
                SessionStorage.saveSession(havingPerformance: label, score: Float(score), AndTimestamp: self.startTimestamp)
            }
            sessionResultLabel.text = label.description
            contactInfoLabel.text = AppConstants.contactInstructionText
            websiteUrlLabel.showWithAnimation(duration: 0.3, havingCompletion: nil)
            sessionResultLabel.showWithAnimation(duration: 0.3, havingCompletion: nil)
            contactInfoLabel.showWithAnimation(duration: 0.3, havingCompletion: nil)
            if SessionConfiguration.shared.shouldDisplayScoreAtSessionEnd {
                scoreLabel.text = String(format: "%.2f", score)
                scoreLabel.showWithAnimation(duration: 0.3, havingCompletion: nil)
            }
        }
    }

    func hideSessionResult() {

        startTimestamp = 0
        hrVarianceToAnalyze = []
        sessionterminatedDueToInterruption = false
        scoreLabel.hideWithAnimation(duration: 0.3, havingCompletion: nil)
        websiteUrlLabel.hideWithAnimation(duration: 0.3, havingCompletion: nil)
        contactInfoLabel.hideWithAnimation(duration: 0.3, havingCompletion: nil)
        sessionResultLabel.hideWithAnimation(duration: 0.3) {
            self.currentSessionStage = .idle
            self.setupUI(forStage: .idle)
        }
    }

    var windowSize = 5
    var performanceThreshhold: Double = 0.5
    enum SessionPerformance: Int {
        case good = 0, average, belowAverage, notAGreatOne
        var text: String {
            switch self {
            case .good: return "Good"
            case .average: return "Average"
            case .belowAverage: return "Below Average"
            case .notAGreatOne: return "Not Great"
            }
        }
        var description: String {
            switch self {
            case .good: return "Great Job!\nYou have achieved calmness."
            case .average: return "Almost there!\nWant to achieve more calmness?"
            case .belowAverage: return "Almost there!\nGood time to reduce your stress levels?"
            case .notAGreatOne: return "You have too much on your mind\nWant to achieve clamness?."
            }
        }
        var indicatorColor: UIColor {
            switch self {
            case .good: return UIColor.green
            case .average: return UIColor.orange
            case .belowAverage: return UIColor.brown
            case .notAGreatOne: return UIColor.red
            }
        }
    }
    func getSessionPerformance(forHRVariance hrVariances: [Double]) -> (Double, SessionPerformance) {
        guard hrVariances.count >= windowSize else {
            return (-1.0, .notAGreatOne)
        }
        var averageHRVariance: Double = 0
        var sum: Double = 0
        for index in (hrVariances.count - windowSize)..<hrVariances.count {
            sum += hrVariances[index]
        }
        averageHRVariance = sum / Double(windowSize)
        if averageHRVariance < 0.25 {
            return (averageHRVariance, .good)
        } else if averageHRVariance < 0.5 {
            return (averageHRVariance, .average)
        } else if averageHRVariance < 0.75 {
            return (averageHRVariance, .belowAverage)
        } else {
            return (averageHRVariance, .notAGreatOne)
        }
    }

}

var count = 0
extension SessionVC: RingHeartRateDelegate {

    func stoppedGettingHeartRates() {

    }


    func heartRateRecieved(heartRate: Double, timeStamp: Double) {
        print(heartRate)
        if currentSessionStage == .idle {
            if isWaitingBeforeNewSession {
                print("Waiting.........")
            } else {
                if Ring.shared.isFingerDetected() {
                    self.currentSessionStage = .preparing
                    self.setupUI(forStage: .preparing)
                }
            }
        } else if currentSessionStage == .ongoing {
            updateSineWave(for: heartRate)
        }
    }

}

extension SessionVC: RingSignalDelegate {

    func bluetoothTurnedOff() {
        print("//////BLUETOOTH TURNED OFF")
    }

    func bluetoothTurnedOn() {
        print("//////BLUETOOTH TURNED ON")
    }

    func ringDisconnected() {
        print("//////RING DISCONNECTED")
    }

    func ringConnected() {
        print("//////RING CONNECTED")
    }

    func fingerNotDetected() {
        print("//////FINGER NOT DETECTED")
        Ring.stopGettingHeartRates()
    }

    func fingerDetected() {
        print("//////FINGER DETECTED")
        Ring.startGettingHeartRates()
    }

    func startedGettingSignal() {
        print("//////SIGNAL RECEIVED")
    }

    func stoppedGettingSignal() {
        print("//////NO SIGNAL")
    }

}

extension UIView {

    func hideWithAnimation(duration: TimeInterval, havingCompletion completion: (() -> Void)?) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0.0
        }) { (completed) in
            if completed {
                completion?()
            }
        }
    }

    func showWithAnimation(duration: TimeInterval, havingCompletion completion: (() -> Void)?) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 1.0
        }) { (completed) in
            if completed {
                completion?()
            }
        }
    }

    func fillSuperview(padding: UIEdgeInsets = .zero) {
        translatesAutoresizingMaskIntoConstraints = false
        if let superviewTopAnchor = superview?.topAnchor {
            topAnchor.constraint(equalTo: superviewTopAnchor, constant: padding.top).isActive = true
        }

        if let superviewBottomAnchor = superview?.bottomAnchor {
            bottomAnchor.constraint(equalTo: superviewBottomAnchor, constant: -padding.bottom).isActive = true
        }

        if let superviewLeadingAnchor = superview?.leadingAnchor {
            leadingAnchor.constraint(equalTo: superviewLeadingAnchor, constant: padding.left).isActive = true
        }

        if let superviewTrailingAnchor = superview?.trailingAnchor {
            trailingAnchor.constraint(equalTo: superviewTrailingAnchor, constant: -padding.right).isActive = true
        }
    }
}

