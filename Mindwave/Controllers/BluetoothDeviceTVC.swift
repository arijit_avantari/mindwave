//
//  BluetoothDeviceTVC.swift
//  Mindwave
//
//  Created by Arijit Sarkar on 15/11/19.
//  Copyright © 2019 Avantari. All rights reserved.
//

import UIKit

class BluetoothDeviceTVC: UITableViewCell {

    var connectionState: BluetoothDevice.ConnectionState = .disconnected {
        didSet {
            connectionStateChanged(toState: connectionState)
        }
    }
    static let identifier = "deviceCell"

    @IBOutlet weak var deviceNameLabel: UILabel!
    @IBOutlet weak var deviceConnectionStatusLabel: UILabel!
    @IBOutlet weak var deviceConnectionChangeActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var rssiLabel: UILabel!

    override func prepareForReuse() {
        super.prepareForReuse()
        connectionState = .disconnected
    }

    private func connectionStateChanged(toState: BluetoothDevice.ConnectionState) {
        switch toState {
        case .connected, .disconnected:
            deviceConnectionStatusLabel.text = toState.text
            deviceConnectionStatusLabel.textColor = toState == .connected ? UIColor.systemBlue : UIColor.gray
            deviceConnectionStatusLabel.alpha = 1.0
            deviceConnectionChangeActivityIndicator.stopAnimating()
        case .connecting, .disconnecting:
            deviceConnectionStatusLabel.alpha = 0.0
            deviceConnectionChangeActivityIndicator.startAnimating()
        }
    }

}
