//
//  HVariance.swift
//  Mindwave
//
//  Created by Arijit Sarkar on 15/11/19.
//  Copyright © 2019 Avantari. All rights reserved.
//

import Foundation

class HVariance {

    static let sharedHVariance:HVariance = HVariance()
    var heartbeatValues = [Double]()
    var XMeanSquareDataArr = [Double]()
    var disturbancesDict:Dictionary = [String:Double]() as Dictionary
    var distubancesCount = 0
    var disturbanceCountPerMiunte = 0
    var deepMeditationCount = 0
    var neitherDeepNorDisturbanceCountperMin = 0

    func varianceOfHeartBeat(heartBeat:Double) -> Double {

        heartbeatValues.append(heartBeat)
        if heartbeatValues.count > 3 {
            let m = meanOfX(index: heartbeatValues.count - 1, arr: heartbeatValues)
            let t = getXMeanSquare(x: heartbeatValues[heartbeatValues.count - 1], mean: m)
            XMeanSquareDataArr.append(t)
            if XMeanSquareDataArr.count > 3 {
                let pm = meanOfX(index: XMeanSquareDataArr.count - 1, arr: XMeanSquareDataArr)
                if pm > 6{
                    distubancesCount = distubancesCount + 1
                    disturbanceCountPerMiunte = disturbanceCountPerMiunte + 1

                }else if pm > 2 && pm <= 6 {

                    neitherDeepNorDisturbanceCountperMin = neitherDeepNorDisturbanceCountperMin + 1
                }
                return pm
            }
        }
        return 0.0
    }

    func getHeartRateVariance(from heartRate: Double) -> Double
    {
        let variance = self.varianceOfHeartBeat(heartBeat: heartRate)
        let dbls = normalizedPowerLevelFromHearbeat(variancesq: variance)
        return Double(dbls)
    }

    func normalizedPowerLevelFromHearbeat(variancesq:Double) -> Double {
        return sqrt(variancesq)/10
    }

    func meanOfX(index:Int,arr:Array<Double>) -> Double {

        //get the array of 4 items from MainArray
        let lastFourHeartBeats = getLastFourItemsFromHeartBeatDataArr(index: index,arr:arr)
        return averageOf(numbers: lastFourHeartBeats)

    }

    func getLastFourItemsFromHeartBeatDataArr(index:Int,arr:Array<Double>) -> [Double] {

        var count:Int = 0
        var returnedArray:Array<Double> = Array()
        while(count <= 3) {

            returnedArray.append(arr[index-count])
            count += 1

        }

        return returnedArray
    }

    func getXMeanSquare(x:Double, mean:Double) -> Double{

        return (x-mean)*(x-mean)

    }


    func averageOf(numbers: [Double]) -> Double {
        let numberTotal = numbers.count
        if numberTotal == 0 {
            return 0
        }
        var sum:Double = 0

        for number in numbers {
            sum += number
        }
        return Double(sum)/Double(numberTotal)
    }

}
