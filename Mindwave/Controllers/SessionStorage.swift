//
//  SessionStorage.swift
//  Mindwave
//
//  Created by Arijit Sarkar on 22/11/19.
//  Copyright © 2019 Avantari. All rights reserved.
//

import Foundation

struct SessionData: Codable {

    var performance: Int
    var score: Float = -1
    var timestamp: Double
    
    enum CodingKeys: String, CodingKey {
        case performance = "performance"
        case score = "score"
        case timestamp = "timestamp"
    }

    init(performance: Int, score: Float, timestamp: Double) {
        self.performance = performance
        self.timestamp = timestamp
        self.score = score
    }
}

class SessionStorage {

    static let key = "AllSessionData"

    static func saveSession(havingPerformance performance: SessionVC.SessionPerformance, score: Float, AndTimestamp timestamp: Double) {
        let data = SessionData(performance: performance.rawValue, score: score, timestamp: timestamp)
        var allSessions = getAllSessions()
        allSessions.append(data)
        let encodedData = try? JSONEncoder().encode(allSessions)
        UserDefaults.standard.set(encodedData, forKey: key)
        UserDefaults.standard.synchronize()
    }

    static func getAllSessions() ->  [SessionData] {
        do {
            guard let data = UserDefaults.standard.value(forKey: key) as? Data else { return [] }
            let decodedData = try JSONDecoder().decode([SessionData].self, from: data)
            return decodedData
        } catch let e {
            print(e.localizedDescription)
        }
        return []
    }
}
