//
//  SettingsVC.swift
//  Mindwave
//
//  Created by Arijit Sarkar on 31/12/19.
//  Copyright © 2019 Avantari. All rights reserved.
//

import UIKit

class SessionConfiguration {

    static let shared = SessionConfiguration()

    let phaseShiftKey = "Phase_Shift_Key"
    var sineWavePhaseShiftValue: Double {
        get {
            guard let val = UserDefaults.standard.value(forKey: phaseShiftKey) as? Double else { return -0.1 }
            return val
        } set {
            UserDefaults.standard.set(newValue, forKey: phaseShiftKey)
            UserDefaults.standard.synchronize()
        }
    }

    let timeWeightKey = "Time_Weight_Key"
    var timeWeightageInResult: Double {
        get {
            guard let val = UserDefaults.standard.value(forKey: timeWeightKey) as? Double else { return 0.6 }
            return val
        } set {
            UserDefaults.standard.set(newValue, forKey: timeWeightKey)
            UserDefaults.standard.synchronize()
        }
    }

    let displayScoreKey = "Display_Score_Key"
    var shouldDisplayScoreAtSessionEnd: Bool {
        get {
            guard let val = UserDefaults.standard.value(forKey: displayScoreKey) as? Bool else { return false }
            print("Returning - Score display - \(val)")
            
            return val
        } set {
            print("Saving - Score display - \(newValue)")
            UserDefaults.standard.set(newValue, forKey: displayScoreKey)
            UserDefaults.standard.synchronize()
        }
    }

    let displayHRKey = "Display_HR_Key"
    var shouldDisplayHRDuringSession: Bool {
        get {
            guard let val = UserDefaults.standard.value(forKey: displayHRKey) as? Bool else { return false }
            print("Returning - HR display - \(val)")
            return val
        } set {
            print("Saving - HR display - \(newValue)")
            UserDefaults.standard.set(newValue, forKey: displayHRKey)
            UserDefaults.standard.synchronize()
        }
    }

    func description() {
        print("\n////////////////////////SETTINGS/////////////////////////")
        print("Phase Shift Val : \(sineWavePhaseShiftValue)")
        print("Time Weight : \(timeWeightageInResult)")
        print("Display Score At End : \(shouldDisplayScoreAtSessionEnd)")
        print("Display HR : \(shouldDisplayHRDuringSession)")
        print("/////////////////////////////////////////////////////////\n")
    }
}

enum ScreenRefreshRate: Int {
    case sixtyHZ = 0, oneTwentyHZ
    static func getScreenRefreshRate(forValue value: Int) -> ScreenRefreshRate? {
        return ScreenRefreshRate(rawValue: value)
    }
    static func getScreenRefreshRate(forSineWaveValue value: Double) -> ScreenRefreshRate? {
        switch value {
        case -0.1: return .sixtyHZ
        case -0.05: return .oneTwentyHZ
        default: return nil
        }
    }
    var valueForSineWave: Double {
        switch self {
        case .sixtyHZ: return -0.1
        case .oneTwentyHZ: return -0.05
        }
    }
}

enum TimeWeigthage: Int {
    case zero = 0, twenty, fourty, sixty, eighty, hundred
    static func getTimeWeightage(forValue value: Int) -> TimeWeigthage? {
        return TimeWeigthage(rawValue: value)
    }
    static func getTimeWeightage(forWeight value: Double) -> TimeWeigthage? {
        switch value {
        case 0.0: return .zero
        case 0.2: return .twenty
        case 0.4: return .fourty
        case 0.6: return .sixty
        case 0.8: return .eighty
        case 1.0: return .hundred
        default: return nil
        }
    }
    var weight: Double {
        switch self {
        case .zero: return 0.0
        case .twenty: return 0.2
        case .fourty: return 0.4
        case .sixty: return 0.6
        case .eighty: return 0.8
        case .hundred: return 1.0
        }
    }
}

class SettingsVC: UIViewController {

    @IBOutlet weak var screenRefreshRateSegmentedControl: UISegmentedControl!
    @IBOutlet weak var timeWeightageSegmentedControl: UISegmentedControl!
    @IBOutlet weak var displayResultScoreSegmentedControl: UISegmentedControl!
    @IBOutlet weak var displayHeartRateSegmentedControl: UISegmentedControl!
    @IBOutlet weak var screenRefreshBGView: UIView!
    @IBOutlet weak var timeWeightageBGView: UIView!
    @IBOutlet weak var displayResultScoreBGView: UIView!
    @IBOutlet weak var displayHRBGView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        setupUI()
    }

    @IBAction func backTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }

    private func initialSetup() {
        screenRefreshRateSegmentedControl.addTarget(self, action: #selector(didChangeScreenRefreshRate), for: .valueChanged)
        timeWeightageSegmentedControl.addTarget(self, action: #selector(didChangeTimeWeightageOnResult), for: .valueChanged)
        displayResultScoreSegmentedControl.addTarget(self, action: #selector(didChangeDisplayStatusOfResultScore), for: .valueChanged)
        displayHeartRateSegmentedControl.addTarget(self, action: #selector(didChangeDisplayStatusOfHeartRate), for: .valueChanged)
    }

    private func setupUI() {

        [screenRefreshBGView, timeWeightageBGView, displayResultScoreBGView, displayHRBGView].forEach {
            $0?.layer.cornerRadius = 10
            $0?.layer.masksToBounds = true
        }

        let sineWaveVal = SessionConfiguration.shared.sineWavePhaseShiftValue
        guard let refreshRate = ScreenRefreshRate.getScreenRefreshRate(forSineWaveValue: sineWaveVal) else { return }
        screenRefreshRateSegmentedControl.selectedSegmentIndex = refreshRate.rawValue

        let timeWeightVal = SessionConfiguration.shared.timeWeightageInResult
        guard let timeWeightage = TimeWeigthage.getTimeWeightage(forWeight: timeWeightVal) else { return }
        timeWeightageSegmentedControl.selectedSegmentIndex = timeWeightage.rawValue

        let shouldDisplayScoreAtSessionEnd = SessionConfiguration.shared.shouldDisplayScoreAtSessionEnd
        displayResultScoreSegmentedControl.selectedSegmentIndex = shouldDisplayScoreAtSessionEnd ? 0 : 1

        let shouldDisplayHRDuringSession = SessionConfiguration.shared.shouldDisplayHRDuringSession
        displayHeartRateSegmentedControl.selectedSegmentIndex = shouldDisplayHRDuringSession ? 0 : 1

        SessionConfiguration.shared.description()
    }

    @objc private func didChangeScreenRefreshRate() {
        let index = screenRefreshRateSegmentedControl.selectedSegmentIndex
        guard let refreshRate = ScreenRefreshRate.getScreenRefreshRate(forValue: index) else { return }
        SessionConfiguration.shared.sineWavePhaseShiftValue = refreshRate.valueForSineWave
        SessionConfiguration.shared.description()
    }

    @objc private func didChangeTimeWeightageOnResult() {
        let index = timeWeightageSegmentedControl.selectedSegmentIndex
        guard let timeWeightage = TimeWeigthage.getTimeWeightage(forValue: index) else { return }
        SessionConfiguration.shared.timeWeightageInResult = timeWeightage.weight
        SessionConfiguration.shared.description()
    }

    @objc private func didChangeDisplayStatusOfResultScore() {
        let index = displayResultScoreSegmentedControl.selectedSegmentIndex
        SessionConfiguration.shared.shouldDisplayScoreAtSessionEnd = index == 0
        SessionConfiguration.shared.description()
    }

    @objc private func didChangeDisplayStatusOfHeartRate() {
        let index = displayHeartRateSegmentedControl.selectedSegmentIndex
        SessionConfiguration.shared.shouldDisplayHRDuringSession = index == 0
        SessionConfiguration.shared.description()
    }

}

