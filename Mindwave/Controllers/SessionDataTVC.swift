//
//  SessionDataTVC.swift
//  Mindwave
//
//  Created by Arijit Sarkar on 23/11/19.
//  Copyright © 2019 Avantari. All rights reserved.
//

import UIKit

class SessionDataTVC: UITableViewCell {

    static let identifier = "sessionCell"
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var performanceLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    
}
